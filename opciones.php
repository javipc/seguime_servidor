<?php
	
	session_start();


	if (isset($_SESSION['usuario'])) 
		$usuario = $_SESSION['usuario'];
	else {
		header('Location: index.php?usuario=demo');
		die();
	}
			
	
	require_once('base.php');
	require_once('faphp.php');
	
	
	
	// conecta con la base de datos
	$bd = conectar();
	
	
		
	
			$bdpanel = new BDDTabla ('panel', $bd);
			$bdpanel->condicion('usuario', $usuario);
			$bdpanel->seleccionar();
			

			$refresco = 0;
			$reg = $bd->siguienteTupla();
			if (isset ($reg['refrescar']))
				$refresco = $reg['refrescar'];
			







	
	
	
	
	
	
	
	

			// visualiza la pagina
?>
		
<html>
	<head>
		<title>Seguime</title>
		<link rel="stylesheet" type="text/css" media="screen" href="esquema.css" />
		<link rel="stylesheet" type="text/css" media="screen" href="panel.css"  />
		<link rel="stylesheet" type="text/css" media="screen" href="movil.css"  />
		
		<meta name=viewport content="width=device-width, initial-scale=1" />
		<meta http-equiv="Content-Type" content="text/html;charset=utf8" />
		<link rel="shortcut icon" href="icono.png">
		
		
	
			
		
	</head>
	
	<body>
			
		<div class='encabezado' >
			
				<span class=icono>
				<?php iconosvg(); ?>
				</span>
			
			
				<h1>Seguime</h1>
			
			<span>
				<a href='panel.php' > 
				<?php 
					$imagen = new faphp ();
					$imagen->altura('30px');
					$imagen->color('#fff');
					$imagen->mostrar ('arrow-left');
				?>
				</a>
			</span>
			
			<span>
				<a href='panel.php?control=cerrarsesion' class = 'rojofuerte'> 
				<?php 					
					$imagen->mostrar ('times');
				?>
				</a>
			</span>
			
			
			
		</div>
		
		
		
		
		<div class="cuerpo">
		
<?php		


			
			// consulta de coordenadas
			$bdpanel = new BDDTabla ('panel', $bd);
			$bdpanel->condicion('usuario', $usuario);
				
						
			

			$bdpanel->seleccionar();
			
			
	$bd->cerrar();
	
	
	
	
?>
		

		<div class="formulario redondeado">
			<fieldset>
				<legend> Opciones </legend>
				
					<form method="post" action="panel.php">
						<label for  = 'actualizarmapa'>Actualizar pantalla (minutos)</label>
						
						<input type = 'number'   name = 'parametro'  id = 'actualizar' maxlength='5' min='0' <?php echo "value='$refresco'"; ?> />
						<input type = 'hidden'   name = 'control' value = 'opciones'  />
						<input type = 'hidden'   name = 'comando' value = 'actualizar' />
						<input type = 'submit'  value = 'Guardar'  />
						
						<p class='nota'>0 (cero) para deshabilitar</p>
					</form>
				
			</fieldset>
		
		
		
		
		
			<fieldset>
				<legend> Configuración de la Aplicación </legend>
					<form method="post" action="panel.php">
						<label for  = 'rastreo'>Habilitar Rastreo</label>
						<input type = 'checkbox' name = 'parametro'  id = 'rastreo' class='interruptor' />
						<input type = 'hidden'   name = 'control' value = 'remoto'  />
						<input type = 'hidden'   name = 'comando' value = 'rastreo' />
						<input type = 'submit'  value = 'Enviar' />
					</form>
					
					<form method="post" action="panel.php">
						<label for  = 'bloqueo'>Habilitar Bloqueo</label>
						<input type = 'checkbox' name = 'parametro'  id = 'bloqueo' class='interruptor' />
						<input type = 'hidden'   name = 'control' value = 'remoto'  />
						<input type = 'hidden'   name = 'comando' value = 'bloqueo' />
						<input type = 'submit'  value = 'Enviar'  />
						
					</form>
					
									
					
						
					<form method="post" action="panel.php">
						<label for  = 'sms'>Número SMS</label>
						<input type = 'text'     name = 'parametro'   id='sms' maxlength='50' placeholder = '114nnnnnnn' />
						<input type = 'hidden'   name = 'control' value = 'remoto' />
						<input type = 'hidden'   name = 'comando' value = 'sms'    />
						<input type = 'submit'  value = 'Enviar' />
						
					</form>
					
					<form method="post" action="panel.php">
						<label for  = 'telegram'>ID Telegram</label>
						<input type = 'text'     name = 'parametro'  id = 'telegram' maxlength='100' placeholder= '175nnnnnnn' />
						<input type = 'hidden'   name = 'control' value = 'remoto'   />
						<input type = 'hidden'   name = 'comando' value = 'telegram' />
						<input type = 'submit'  value = 'Enviar' />
					</form>
				</fieldset>
				
			</div>
		<div class=ayuda>
			Ayuda
			<p>
				<strong>Rastreo</strong> Habilita el envío de mensajes a través de SMS y Telegram cuando se detectan nuevas coordenadas.
			</p>
			<p>
				<strong>Bloquear</strong> Impide el acceso al registro de coordenadas, configuración y no permite deshabilitar el servicio. 				.
			</p>						
			<p>
				<strong>SMS / Telegram</strong> Si el "rastreo" esta habilitado enviará mensajes a estos destinatarios
				(Por seguridad, cualquier número de SMS que introduzca borrará el número SMS de la aplicación, edita la aplicación si deseas configurar este parámetro)
			</p>
							
		</div>
		
		<div class = eliminarcuenta>
			</p>
				<a href="eliminar.php" class="boton rojo">Eliminar Cuenta</a>
				<a href="eliminar.php" class="boton rojo">Eliminar Datos</a>
			</p>
			
		</div>

		
		</div>

	</body>
</html>
			
		

	
