





















<html>
	<head>
		<title>Seguime - Base de Datos</title>
		
		<meta name=viewport content="width=device-width, initial-scale=1" />
		<meta http-equiv="Content-Type" content="text/html;charset=utf8" />
		
		
		<link rel="stylesheet" type="text/css" media="screen" href="esquema.css" />
		<link rel="stylesheet" type="text/css" media="screen" href="panel.css"  />
	</head>
	
	<body>			
	<div class='encabezado' >
		
			
			
			<span class=icono>
				<?php require_once('base.php');
				iconosvg(); ?>
				</span>
		
		<h1>Seguime</h1>
		</div>
	
	<div class= "centrado">
		<p class= "mensaje rojo">
			ELIMINAR ESTE ARCHIVO <br/>
			NO DEJAR ESTE ARCHIVO EN EL SERVIDOR
		</p>
	</div>
	
					
<?php
	
	

	/*
		Genera base de datos completa para utilizar con la aplicación.
	
	*/
	
	
	
	$direccion = bd_dato('direccion');
	$usuario   = bd_dato('usuario');
	$clave     = bd_dato('clave');
	$base      = bd_dato('nombre');
	
	








	

	echo " <br/>Creando base de datos: $base ...";
	$bd = new BDD ($base, $usuario, $clave, $direccion);
	$bd->visor();
	$bd->crear();
	$bd->conectar ();
	
		// Tabla para almacenar comandos enviados a la aplicación
	
		$instrucciones = new BDDTabla ('instrucciones', $bd);
		
		$instrucciones->tipo_columna('id'       , 'id');
		$instrucciones->tipo_columna('usuario'  , 'caracteres');
		$instrucciones->tipo_columna('comando'  , 'caracteres');
		$instrucciones->tipo_columna('parametro', 'caracteres');
		$instrucciones->tipo_columna('enviado'  , 'entero');
	
	
		// Tabla para almacenar coordenadas recibidas por la aplicación
		
		$coordenadas = new BDDTabla ('coordenadas', $bd);
	
		$coordenadas  ->tipo_columna('id'       , 'id');
		$coordenadas  ->tipo_columna('usuario'  , 'caracter50');
		$coordenadas  ->tipo_columna('latitud'  , 'caracter50');
		$coordenadas  ->tipo_columna('longitud' , 'caracter50');
		$coordenadas  ->tipo_columna('fecha'    , 'fecha');
		$coordenadas  ->tipo_columna('velocidad', 'caracter50');
		$coordenadas  ->tipo_columna('proveedor', 'caracter50');
		$coordenadas  ->tipo_columna('extra'    , 'texto');
				
		
		// usuarios	
		$usuarios = new BDDTabla ('usuarios', $bd);
			  
		$usuarios ->tipo_columna('id'     , 'id');
		$usuarios ->tipo_columna('usuario', 'caracteres');
		$usuarios ->tipo_columna('clave'  , 'caracteres');
		$usuarios ->tipo_columna('fecha'  , 'fecha');
		$usuarios ->tipo_columna('prueba' , 'caracteres');
			
			
		// Tabla para almacenar los intentos de ingreso y registro
		
		$accesos = new BDDTabla('accesos' , $bd);
		
		$accesos  ->tipo_columna('id'     , 'id');						 
		$accesos  ->tipo_columna('ip'     , 'caracteres');						 
		$accesos  ->tipo_columna('archivo', 'caracteres');						 
		$accesos  ->tipo_columna('accion' , 'caracteres');						 
		$accesos  ->tipo_columna('bloqueo', 'entero');						 
		$accesos  ->tipo_columna('fecha'  , 'fecha');						 
		
		
		// Guarda una copia de la alarma
		
		$alertas = new BDDTabla('alertas', $bd);
		
		$alertas  ->tipo_columna('id'         , 'id');
		$alertas  ->tipo_columna('usuario'    , 'caracteres');
		$alertas  ->tipo_columna('mensaje'    , 'caracteres');
		$alertas  ->tipo_columna('vencimiento', 'fecha');
		
		
		// Contactos para enviar mensajes de alerta
		
		$contactos = new BDDTabla ('contactos', $bd);
		
		$contactos->tipo_columna('id'      , 'id');
		$contactos->tipo_columna('usuario' , 'caracteres');
		$contactos->tipo_columna('dato'    , 'caracteres');
		$contactos->tipo_columna('tipo'    , 'caracteres');
		

		// Configuración web y otros datos
		
		$panel = new BDDTabla ('panel', $bd);
		
		$panel->tipo_columna('id'        , 'id');
		$panel->tipo_columna('usuario'   , 'caracteres');
		$panel->tipo_columna('visto'     , 'fecha');
		$panel->tipo_columna('refrescar' , 'entero');


		// Fotografías
		
		$imagen = new BDDTabla ('imagen', $bd);
		
		$imagen->tipo_columna('id'      , 'id');
		$imagen->tipo_columna('usuario' , 'caracteres');
		$imagen->tipo_columna('fecha'   , 'fecha');
		$imagen->tipo_columna('imagen'  , 'texto');
		$imagen->tipo_columna('extra'   , 'caracteres');
		
		
		
		echo " <br/>Creando tablas y columnas...";

		$usuarios     ->crearTablaYColumnas();
		$coordenadas  ->crearTablaYColumnas();
		$alertas      ->crearTablaYColumnas();
		$instrucciones->crearTablaYColumnas();
		$contactos    ->crearTablaYColumnas();
		$accesos      ->crearTablaYColumnas();
		$panel        ->crearTablaYColumnas();
		$imagen       ->crearTablaYColumnas();


	
	
	echo ' <br/> Operación finalizada';
	echo ' <br/> Información de las tablas en la base de datos';
		
		$usuarios     ->infoTabla()->imprimir("class='table'");
		$coordenadas  ->infoTabla()->imprimir("class='table'");
		$alertas      ->infoTabla()->imprimir("class='table'");
		$instrucciones->infoTabla()->imprimir("class='table'");
		$contactos    ->infoTabla()->imprimir("class='table'");
		$accesos      ->infoTabla()->imprimir("class='table'");
		$panel        ->infoTabla()->imprimir("class='table'");
		$imagen       ->infoTabla()->imprimir("class='table'");
	
	echo "<div class= 'bloque centrado_unico'>";
			
			

	
		echo "'<p class='mensaje verde'>";
			echo "&iexcl;Listo!";
			echo '<br/> Registre un nuevo usuario desde la aplicación';
			echo '<br/> Si ya existía un usuario puede usarlo';
		echo "</p>";
		echo "<br/> <a href=panel.php class = 'boton verde redondeado'>Ir al Panel</a>";
	
	echo "</div>";


?>

</body>
</html>