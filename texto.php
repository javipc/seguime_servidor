<?php




	function obtenerIdioma () {
		// idioma principal
		$idioma = 'es';

		// obtiene idiomas desde una cookie
		if (isset($_COOKIE['idioma']))
			$idioma = $_COOKIE['idioma'];



		// obtiene idioma desde url o post (descarta la cookie)
		if (isset($_REQUEST['idioma']))
			$idioma = $_REQUEST['idioma'];



		// verifica que sea de idiomas soportados
		switch ($idioma) {
			case 'en-UK':
			case 'en-US':
			case 'en'   :
				$idioma = 'en';
				$_COOKIE['idioma'] = $idioma;
				return $idioma;
				break;

			case 'es-ES':
			case 'es-AR':
			case 'es'   :
				$idioma = 'es';
				$_COOKIE['idioma'] = $idioma;
				return $idioma;
				break;
		}

		// devuelve idioma original
		return $idioma;
	}





	function selectorIdiomas (...$idiomas) {
		echo "<div class = 'idiomas'>";
		foreach ($idiomas as $idioma)
			echo "<div class = 'idioma'><a href=?idioma=$idioma>$idioma</a></div>";
		echo '</div>';
	}









	function p ($texto, $idioma='') {
		echo '<p>';
		t ($texto, $idioma);
		echo '</p>';
	}


	function t ($texto, $idioma='') {
		echo htmlentities ( texto ($texto, $idioma), ENT_QUOTES, 'UTF-8');
	}











	function texto($textoOriginal, $idioma='') {

		$texto = strtolower($textoOriginal);

		if ($idioma == '')
			$idioma = obtenerIdioma();
























		////////////////////////////////////////////////////////////
		// INGLES
		////////////////////////////////////////////////////////////





		if ($idioma == 'en')
			switch ($texto) {

				case 'menu'             : return 'Menu';
				case 'principal'        : return 'Home';
				case 'iniciar sesion'   : return 'Login';
				case 'capturas'         : return  'Screen shots';
				case 'caracteristicas'  : return 'Features';
				case 'descargas'        : return 'Downloads';
				case 'por que'          : return 'Why Seguime?';
				case 'invita un cafe'   : return 'Invite a coffee';
				case 'donaciones'       :
				case 'hace una donacion': return 'make a donation';
				case 'mas info'         : return 'More information';


				// portada
				case 'titulo': return 'Seguime'; // no se traduce
				case 'seguime': return 'Seguime'; // no se traduce
				case 'subtitulo': return 'Seguime is the first truly free application for tracking device without tracking you';



				// sesión

				case 'tu clave': return 'your user name';
				case 'tu usuario': return 'your password';

				// errores
				case 'error usuario largo' : return 'user name is too large';
				case 'error clave larga': return 'password is too large';

				case 'error bdd' : return 'I think the data base is not working';
				case 'error conexiones': return 'you have too many strange conections';
				case 'error usuario o clave incorrecto': return  'wrong user name or password';

				case 'inicar sesion subtitulo': return 'Log in and track your device from here.';




					// capturas

				case 'capturas titulo' : return  'Screen shots';
				case 'capturas subtitulo' : return  'about Seguime screes';

				case 'captura ingreso det' : return  'lon in screen';
				case 'captura principal det' : return  'Main screen';
				case 'captura configuracion det' : return  'Settings';









				// caracteristicas

				case "caracteristicas subtitulo" : return "Seguime is free...";

				case "publicidad": return "Advertising";
				case "publicidad det" : return "Many applications generate more traffic in the downloads of advertising than in the use of the same, Seguime has no advertising.";

				case "servidor propio": return "Own server";
				case "servidor propio det" : return "You can use your own server, for privacy and security of your data, it is also freedom.";

				case "codigo propietario" : return "Proprietary code";
				case "codigo propietario det" : return "Seguime is free, and your code does not have proprietary programs, or for spreading companies.";

				case "rastreadores" : return "Trackers";
				case "rastreadores det" : return "This application nor this website has links to social networks, or analysis services, therefore here crawlers do not exist.";

				case "telemetria" : return "Telemetry";
				case "telemetria det" : return "Not applied any kind of follow-up on your activities, or how you use your device as you use the application, does not nor sends reports of any kind.";

				case "scripts ocultos" : return "Hidden Scripts";
				case "scripts ocultos det": return "Neither this site nor the application activities are hidden such as mining or data collection, there is no malicious script or for profit, all the code is clean.";

				case "codigo libre": return	"Free code";
				case "codigo libre det": return "You can see how this fact, adapt it to your taste or needs, customize it, correct errors, what you want.";


				// descargas

				case "descargas subtitulo": return "Get your application right now";


				case "aplicacion" : return "Aplication";
				case "aplicacion det": return "Get mobile for Android, download application without having to create accounts in stores";

				case "servidor" : return "Server";
				case "servidor det" : return "Get files that make up the server, you can install it locally on your computer or a web hosting, to have your own server you have the comfort of being sure where your data is.";

				case "codigo fuente": return "Source code";
				case "codigo fuente det" : return "Get the files of the source code for the application for android";

				case "descargar" : return "Download";
				case "ir al repositorio": return "Go to the repository";
				case "version" : return "Versión";


				// por que la aplicacion
				case "por que det" : return "There are many applications that make \"the same\", but if we talk about privacy, one should rely on the companies offering those applications, today everyone wants to get your data, therefore spreading companies offer \"Analytics\", advertising, captchas, APIs, maps, VPN all \"free\", to change collected user data.
This happens much in android devices, but there are many applications that do the same, I found none that does not have proprietary code or spreading products, all using Google services, Google Maps, etc. That's why I decided to develop this application, free trackers, and free from proprietary codes";


				// donaciones
				case "apoya a seguime": return "Supports to Seguime";
				case "para que siga libre" : return "To remain free";

				case "invita un cafe": return "invites a coffee :)";

				case "manito extendida": return "
				You are invited to collaborate with this project,
				like any free program, its development depends on the users,
				you can help by donating and/or disseminating the application to be tested by your contacts or visitors to your website.
				Thank you for your help!
				";



				// AYUDA.PHP

				// menu
				case "ayuda" : return "Help";
				case "acerca del proyecto" : return "About the project";
				case "acerca de la aplicacion" : return "About the application";
				case "acerca del servidor": return "About the server";
				case "creditos" : return "Credits";
				case "volver" : return "Back to home";

				// portada

				case "seccion ayuda" : return "Section of help and information";

				case "advertencia proyecto" : return "
					This is a project, it is not (and not pretends to be) a security aplication,
					it is only for experimental use.
					The developer (or developers and collaborators)
					are not and will not be responsible for the use or misure of these programs
					(applications, website, source codes, etc.).

					Esto es un proyecto, no es (ni pretende ser) una aplicación de seguridad,
					es solo para uso experimental.
					El desarrollador (o desarrolladores y colaboradores)
					no son ni serán responsables por el uso o mal uso de estos programas
					(aplicaciones, sitio web, códigos fuentes, etc.).
				";




				// Acerca del Proyecto

				case "que es seguime"  : return "What is Seguime?";
				case "que es seguime det"  : return "Seguime is a free application that stores the GPS and the send data to a server in order to be able to be located";



				case "cuanto cuesta"  : return "How much is this application?";
				case "cuanto cuesta det"  : return "It is free, you can use it freely without cost, if you wish you can make <a href=https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&hosted_button_id=BDUHGWZKV2R8W> a donation here</a> .";





				// Acerca de la Aplicacion



				case "como hacer una cuenta"  : return "How I can make an account?";
				case "como hacer una cuenta det"  : return "Accounts are made in the application, you may chose the option to register, then a username and password, it does not ask personal data.";

				case "por que fuentes desconocidas"  : return "Why uknow sources? ";
				case "por que fuentes desconocidas det"  : return "It is a so-called measure of safety, your device and personal data are more vulnerable to attacks from unknown sources applications, or the less that want you to believe. \"They\" is safer to install the \"Google Play store\" applications, which is not true.";


				case "version whatsapp"  : return "¿Habrá una versión para enviar las coordenadas a WhatsApp, al igual que Telegram?";
				case "version whatsapp det"  : return "Definitivamente ¡NO!.<br/>
					Como dije, nada de empresas rastreadoras (Facebook), nada de código propietario.<br/>";


				// Acerca del Servidor


				case "es hackeable"  : return "Server is hackable?";
				case "es hackeable det"  : return "Yes!. This server is only for show, it is recommended that you use your own server.";

				case "como instalar servidor"  : return "How to install the Server?";
				case "como instalar servidor det"  : return "Copy the files and run db.php, this will create the necessary database, some services may require you believe the database on your website. Ready, and it should work, just set your application to connect to your server.";


				case "que info recolecta"  : return "¿Que información recolecta el sitio web?";
				case "que info recolecta det"  : return "Este sitio (por ahora, y espero que siga siendo así) no tiene publicidad (la versión sin publicidad), ni analíticos, ni botones sociales, ni fuentes ni apis externas, ni servicios webs, ni scripts... nada de \"cosas raras\". <br/>
					Este sitio la única información que recolecta es la dirección IP, y es solo para evitar intentos de inicio de sesiones o registros masivos.
					Esa IP queda en una tabla \"anonimia\", es decir no se la asocia a NINGÚN USUARIO, NI CUENTA.";

				case "datos seguros"  : return "My data produced by the application, are safe in this server?";
				case "datos seguros det"  : return "No! This server is unsafe, FOR EVER!. This server should be used to test the performance of the application, is not the Mission of this server to offer services for the application, so that you have available the files needed to install your own server.";

				case "info eliminada del servidor"  : return "Cuando se elimina la información del servidor ¿se elimina de verdad o se \"marca como eliminada\"?";
				case "info eliminada del servidor det"  : return "En Internet, cuando eliminas algo, nunca se elimina de verdad, solo se \"marca como\" eliminado y así deja de estar visible esa información (ejemplo: cualquier red social que conozcas),
					aquí lo que eliminas, se elimina y no queda rastros,
					por parte del servicio de alojamiento desconozco si realizan copias de seguridad, deberían, pero no lo se.";
				// si no encuentra la traduccion buscara en el idioma original
				default: $idioma = "es-AR";
			}





































		////////////////////////////////////////////////////////////
		// ESPAÑOL - ORIGINAL
		////////////////////////////////////////////////////////////
	if ($idioma == 'es')
			switch ($texto) {

				case 'menu' : return  'Menú';
				case 'principal' : return  'Principal';
				case 'iniciar sesion': return  'Iniciar Sesión';
				case 'capturas' : return  'Capturas';
				case 'caracteristicas': return  'Características';
				case 'descargas': return  'Descargas';
				case 'por que': return  '¿Por qué Seguime?';
				case "invita un cafe": return "Invita un café";
				case 'donaciones': return  'Invita un café :)';
				case "hace una donacion": return "Hacé una donación";
				case 'mas info' : return  'Más Información';


				// portada
				case 'titulo': return  'Seguime'; // no se traduce
				case 'seguime': return 'Seguime'; // no se traduce
				case 'subtitulo': return  'La primer aplicación Libre que te permite rastrear un dispositivo sin que te rastreen a vos';



				// sesión

				case 'tu clave': return  'Tu clave';
				case 'tu usuario': return  'Tu usuario';

				// errores
				case 'error usuario largo' : return  'el nombre de usuario es muy largo';
				case 'error clave larga': return  'la clave es muy larga';

				case 'error bdd' : return  'bueno bueno, parece que la base de datos no está funcionando :( ';
				case 'error conexiones': return  'algo raro está pasando con tu conexión, hay muchos intentos de iniciar sesión, por favor espera unas horas';
				case 'error usuario o clave incorrecto': return   'Usuario o Clave incorrectos';

				case 'inicar sesion subtitulo': return  'Ingresa y rastrea tu dispositivo desde aquí.';






				// capturas

				case 'capturas titulo' : return  'Capturas de Pantalla';
				case 'capturas subtitulo' : return  'Un vistazo a la interfaz de la Aplicación';

				case 'captura ingreso det' : return  'pantalla de inicio de sesión';
				case 'captura principal det' : return  'pantalla principal, aquí se activa y desactiva la aplicación';
				case 'captura configuracion det' : return  'pantalla de configuración';





				// caracteristicas

				case 'caracteristicas subtitulo' : return  'Seguime es Libre...';

				case 'publicidad': return  'Publicidad';
				case 'publicidad det' : return  'Muchas aplicaciones generan más tráfico en la descargas de publicidad que en el uso de la misma, Seguime no tiene publicidad.';

				case 'servidor propio': return  'Servidor Propio';
				case 'servidor propio det' : return  'Podes usar tu propio servidor, para mayor privacidad y seguridad de tus datos, eso también es libertad.';

				case 'codigo propietario' : return  'Código propietario';
				case 'codigo propietario det' : return  'Seguime es Libre, y su código no posee programas propietarios, ni de empresas rastreadoras.';

				case 'rastreadores' : return  'Rastreadores';
				case 'rastreadores det' : return  'Ni la aplicación ni este sitio web tienen enlaces a redes sociales, ni servicios de análisis, por lo tanto aquí los rastreadores no existen. (la versión sin publicidad)';

				case 'telemetria' : return  'Telemetria';
				case 'telemetria det' : return  'La aplicación no realiza ningún tipo de seguimiento sobre tus actividades, ni como usas tus dispositivos ni como usas la aplicación, no realiza ni envía informes de ningún tipo.';

				case 'scripts ocultos' : return  'Scripts Ocultos';
				case 'scripts ocultos det': return  'Ni este sitio ni la aplicación realizan actividades ocultas como minería o recolección de datos, no hay script maliciosos ni con fines lucrativos, todo el código está limpio.';

				case 'codigo libre': return 	'Código Libre';
				case 'codigo libre det': return  'Podes ver como está hecho, adaptarlo a tus gustos y/o necesidades, personalizarlo, corregir errores, lo que desees.';


				// descargas

				case 'descargas subtitulo': return  'Obtené tu aplicación ahora mismo';


				case 'aplicacion' : return  'Aplicación';
				case 'aplicacion det': return  'Obtené la aplicación móvil para Android, descargala sin necesidad de tener que crear cuentas en tiendas.';

				case 'servidor' : return  'Servidor';
				case 'servidor det' : return  'Obtené los archivos que componen el servidor, podes instalarlo en forma local en tu ordenador o en un alojamiento web, al tener tu propio servidor tenés la tranquilidad de estar seguro donde están tus datos.';

				case 'codigo fuente': return  'Código Fuente';
				case 'codigo fuente det' : return  'Obtené los archivos del código fuente de la aplicación para Android.';

				case 'descargar' : return  'Descargar';
				case 'ir al repositorio': return  'Ir al repositorio';
				case 'version' : return  'Versión';


				// descripcion
				case 'descripcion': return  '¿Qué es Seguime?';
				case 'descripcion det' : return  '
					Seguime te permite rastrear tu dispositivo robado,
					localizar tu teléfono perdido,					
					enviar un mensaje en caso de posible peligro.
					Seguime recolecta información del GPS y la almacena para luego poder enviarlas a tu servidor.
					Desde el servidor se puede ver los registros en un mapa y ver donde está (o estuvo) el dispositivo.
					El servidor puede ser el de un amigo, familiar o el tuyo,
					podes montar tu propio servidor para mayor privacidad de tus datos.
					Además incluye un temporizador que te permite programar en que momento la aplicación activará su modo de rastreo
					y adicionalmente enviar un mensaje de ayuda.
					Cuando la aplicación está en modo de rastreo enviará por SMS o Telegram mensajes con la ubicación.
					';



				// por que la aplicacion
				case 'por que det' : return  'Hay muchas aplicaciones que hacen \'lo mismo\', pero si hablamos de privacidad, uno debería confiar en las empresas que ofrecen esas aplicaciones,
						hoy todo el mundo quiere obtener tus datos, por eso las empresas rastreadoras ofrecen \'Análisis\', publicidades, captchas, apis, fuentes, mapas, VPN todo \'gratis\',
						a cambio recolectan datos de los usuarios.
						Esto sucede mucho en Internet (Redes Sociales), y en especial en dispositivos móviles, donde aplicaciones obtienen datos sin que lo sepas (incluso SMS, Llamadas, etc)
						si bien hay muchas aplicaciones que hacen lo mismo, no encontré ninguna que no tenga código propietario ni productos de
						empresas rastreadoras, todas usan Google Maps, Servicios de Google, etc.
						Es por eso que decidí desarrollar esta aplicación, como un proyecto para Android en la Universidad de La Punta, que sea libre de rastreadores, y libre de códigos propietarios.';


				// donaciones
				case 'apoya a seguime': return  'Apoya a Seguime';
				case 'para que siga libre' : return  'Para que siga siendo Libre';

				case 'invita un cafe': return  'Invita un café al programador :)';

				case 'manito extendida': return  '
				Estas invitado a colaborar con el este proyecto,
				al igual que cualquier programa libre, su desarrollo depende de los usuarios,
				podes ayudar haciendo donaciones y/o difundiendo la aplicación para que la prueben tus contactos o visitantes de tu sitio web.
				¡Gracias por tu ayuda!
				';



				// AYUDA.PHP

				// menu
				case 'ayuda' : return  'Ayuda';
				case 'acerca del proyecto' : return  'Acerca del Proyecto';
				case 'acerca de la aplicacion' : return  'Acerca de la Aplicación';
				case 'acerca del servidor': return  'Acerca del Servidor';
				case 'creditos' : return  'Créditos';
				case 'volver' : return  'Volver';

				// portada

				case 'seccion ayuda' : return  'Sección de Ayuda e Información';

				case 'advertencia proyecto' : return  '
					Esto es un proyecto, no es (ni prende ser) una aplicación de seguridad,
					es solo para uso experimental.
					El desarrollador (o desarrolladores y colaboradores)
					no son ni serán responsables por el uso o mal uso de estos programas
					(aplicaciones, sitio web, códigos fuentes, etc.).
				';




				// Acerca del Proyecto

				case 'que es seguime'  : return  '¿Qué es Seguime?';




				// SIN HTMLELTITIES
				case 'cuanto cuesta'  : return  '¿Cuanto cuesta la aplicación?';
				case 'cuanto cuesta det'  : return  'Es libre, podes usarlo libremente sin costos, si lo deseas podes hacer <a href=https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&hosted_button_id=BDUHGWZKV2R8W>colaborar con una donación aquí</a> .';





				// Acerca de la Aplicacion



				case 'como hacer una cuenta'  : return  '¿Cómo se hace una cuenta?';
				case 'como hacer una cuenta det'  : return  'Los registros se realizan a través de la aplicación, elegí la opción de Registro, luego un usuario y clave, no pide datos personales.';

				case 'por que fuentes desconocidas'  : return  '¿Por qué me pide que configure mi teléfono móvil para que pueda instalar aplicaciones de fuentes desconocidas? ';
				case 'por que fuentes desconocidas det'  : return  'Es una supuesta medida de seguridad, tu dispositivo y datos personales son mas vulnerables a los ataques de aplicaciones de fuentes desconocidas, o al menos eso te quieren hacer creer.
					Según \'ellos\' es mas seguro que instales aplicaciones de la tienda \'Google Play Store\', lo cual no es cierto.		';


				case 'version whatsapp'  : return  '¿Habrá una versión para enviar las coordenadas a WhatsApp, al igual que Telegram?';
				case 'version whatsapp det'  : return  'Definitivamente ¡NO!.
					Nada de empresas rastreadoras (Facebook), nada de código propietario. solo software libre.';


				// Acerca del Servidor


				case 'es hackeable'  : return  '¿Es vulnerable?';
				case 'es hackeable det'  : return  '¡SI!. Este servidor es solo para demostración, lo recomendable es que uses tu propio servidor. Si accediste a la base de datos del servidor, luego de que te diviertas podés contactarte para hacer más segura la aplicación.';

				case 'como instalar servidor'  : return  '¿Como instalar el servidor?';
				case 'como instalar servidor det'  : return  'Copiar los ficheros y ejecutar instalar.php, este creara la base de datos necesaria, algunos servicios de alojamiento pueden exigir que crees la base de datos en su sitio.
					Listo, ya debería funcionar, solo configura tu aplicación para que se conecte a tu servidor.';


				case 'que info recolecta'  : return  '¿Que información recolecta el sitio web?';
				case 'que info recolecta det'  : return  'Este sitio (por ahora, y espero que siga siendo así) no tiene publicidad (la versión sin publicidad), ni analíticos, ni botones sociales, ni fuentes ni apis externas, ni servicios webs, ni scripts... nada de \'cosas raras\'.
					Este sitio la única información que recolecta es la dirección IP, y es solo para evitar intentos de inicio de sesiones o registros masivos.
					Esa IP queda en una tabla \'anonimia\', es decir no se la asocia a NINGÚN USUARIO, NI CUENTA.';

				case 'datos seguros'  : return  'Mis datos producidos por la aplicación, ¿son seguros en este servidor?';
				case 'datos seguros det'  : return  '¡NO!
					Este servidor ¡NO ES SEGURO, NI LO SERÁ!. Este servidor solo deberá usarse para probar el funcionamiento de la aplicación,
					no es la misión de este servidor ofrecer servicios para la aplicación,
					es por eso que tenés disponible los archivos necesarios para poder instalar tu propio servidor.';

				case 'info eliminada del servidor'  : return  'Cuando se elimina la información del servidor ¿se elimina de verdad o se \'marca como eliminada\'?';
				case 'info eliminada del servidor det'  : return  'En Internet, cuando eliminas algo, nunca se elimina de verdad, solo se \'marca como\' eliminado y así deja de estar visible esa información (ejemplo: cualquier red social que conozcas),
					aquí lo que eliminas, se elimina y no queda rastros,
					por parte del servicio de alojamiento desconozco si realizan copias de seguridad, deberían, pero no lo se.';




				// registro de aplicacion

				case 'su usuario'  : return  'Usuario';
				case 'registrar aplicacion'  : return  'Registrar Aplicación';
				case 'su clave es'  : return  'Su clave de registro es';
				case 'registro de aplicacion'  : return  'Registro de Aplicación';







				// borrar cuenta
				case 'eliminar cuenta'  : return  'Eliminar Cuenta';





			}

				return $textoOriginal;

	}






















?>
