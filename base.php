<?php

	require_once("texto.php");
	require_once ('configuracion_bdd.php');

	require_once ('jbdd.php');



	

	// CONFIGURAR LA ZONA HORARIA SEGÚN https://www.php.net/manual/es/class.datetimezone.php
	// date_default_timezone_set('America/Argentina/Buenos_Aires');
	date_default_timezone_set('UTC');




	// FUNCIONES





	// base de datos
	function conectar () {

		$direccion = bd_dato ('direccion');
		$usuario   = bd_dato ('usuario');
		$clave     = bd_dato ('clave');
		$base      = bd_dato ('nombre');

		$bd = new BDD ($base, $usuario, $clave, $direccion);
		$bd->conectar ();
		$bd->utf8     ();
		return $bd;
	}








	// formularios
	function dato ($etiqueta='comando', $valor='') {
		if (isset($_REQUEST[$etiqueta]))
			return sanar($_REQUEST[$etiqueta]);
		else
			return $valor;
	}





	// obtiene fecha y hora
	function ahora () {
		return date('Y-m-d H:i:s');
	}
	function fecha($fecha) {
		return  date('d-m-Y', strtotime($fecha));
	}

	function zonaHoraria () {
		return  date_offset_get(new DateTime) / 3600;
	}



















	//-----------------------------------------------------------------------
	// CADENAS Y VARIABLES ----------------------------------------------------------
	//-----------------------------------------------------------------------



	// obtiene palabras de una cadena
	function nropalabras ($cadena) {
		//foreach ( str_word_count($str, 1, 'àáãçñÑáéíóúüàèìòù1234567890') as $x){ echo "<br/> $x "; }
		return  str_word_count($cadena, 1, 'àáãçñÑáéíóúüàèìòù1234567890') ;
	}

	function palabras ($cadena) {
		 $resultado=array();
		 $contador =0;

		$c=' \n\t';
		$tok = strtok($cadena, $c);

		while ($tok !== false) {
			$tok = strtok($c);
			$resultado['$contador'] = $tok;
			$contador=$contador+1;
		}

		return $resultado;
	}


	// quita tildes
	function tildes($s) {
		$vocalti= array ('á','é','í','ó','ú','Á','É','Í','Ó','Ú','À','È','Ì','Ò','Ù','à','è','ì','ò','ù','ç','Ç','â','ê','î','ô','û','Â','Ê','Î','Ô','Û','ü','ö','Ö','ï','ä','ë','Ü','Ï','Ä','Ë');
		$vocales= array ('a','e','i','o','u','A','E','I','O','U','A','E','I','O','U','a','e','i','o','u','c','C','a','e','i','o','u','A','E','I','O','U','u','o','O','i','a','e','U','I','A','E');
		$s=str_replace($vocalti, $vocales,$s);
		return $s;
	}

	// quita caracteres extraños
	function caracteres($s) {
		$vocalti= array ('#','á','é','í','ó','ú','Á','É','Í','Ó','Ú','À','È','Ì','Ò','Ù','à','è','ì','ò','ù','ç','Ç','â','ê','î','ô','û','Â','Ê','Î','Ô','Û','ü','ö','Ö','ï','ä','ë','Ü','Ï','Ä','Ë');
		$vocales= array (' ','a','e','i','o','u','A','E','I','O','U','A','E','I','O','U','a','e','i','o','u','c','C','a','e','i','o','u','A','E','I','O','U','u','o','O','i','a','e','U','I','A','E');
		$s=str_replace($vocalti, $vocales,$s);
		return $s;
	}

	// convierte cadena a compatible con urls
	function url($s) {
		$vocalti= array (' ');
		$vocales= array ('%20');
		$s=str_replace($vocalti, $vocales,$s);

		$s=filter_var($s, FILTER_SANITIZE_URL);
		return $s;
	}

	// quita los espacios
	function sinespacio ($s) {
		$vocalti= array (' ');
		$vocales= array ('-');
		$s=str_replace($vocalti, $vocales,$s);

		$s=filter_var($s, FILTER_SANITIZE_URL);
		return $s;
	}





	// convierte a minusculas
	function minusculas($s) {
	$s=strtolower($s);
	return $s;
	}

	// valida variables
	function sanar ($s) {
		$s=filter_var($s, FILTER_SANITIZE_STRING);
		return $s;
	}

	// valida variables enteras
	function entero ($s) {
		$s=filter_var($s, FILTER_VALIDATE_INT);
		return $s;
	}


	// url amigable
	function amigable ($cadena) {
		$cadena = sanar      ($cadena);
		$cadena = minusculas ($cadena);
		$cadena = caracteres ($cadena);
		$cadena = sinespacio ($cadena);
		return $cadena;
	}




	// ------------------------------------------





	function obtenerip () {
		if (isset($_SERVER['HTTP_CLIENT_IP']))       return $_SERVER['HTTP_CLIENT_IP'];
		if (isset($_SERVER['HTTP_X_FORWARDED_FOR'])) return $_SERVER['HTTP_X_FORWARDED_FOR'];
		if (isset($_SERVER['HTTP_X_FORWARDED']))     return $_SERVER['HTTP_X_FORWARDED'];
		if (isset($_SERVER['HTTP_FORWARDED_FOR']))   return $_SERVER['HTTP_FORWARDED_FOR'];
		if (isset($_SERVER['HTTP_FORWARDED']))       return $_SERVER['HTTP_FORWARDED'];
		return    $_SERVER['REMOTE_ADDR'];
	}








	function ipbloqueada ($archivo, $accion, $bd) {
		$ip    = obtenerip();
		$ahora = ahora();
		$denegacion = 0;
		$maximoregistroporip = 3;
		$maximoaingresoporip =10;
		$maximousuarioincorrectoporip = 5;

		$tiempo = date('Y-m-d H:i:s',strtotime ('$ahora -1 hour'));


		$bdacceso = new BDDTabla ('accesos', $bd);
		$bdacceso->condicion ('ip', $ip);
		$bdacceso->condicion ('fecha', '>', $tiempo);

		$totalbloqueo   = 0;
		$totalregistros = 0;
		$totalingresos  = 0;
		$totalusuarioincorrecto = 0;

		while ($respuesta = $bd->siguienteTupla()) {
			$totalbloqueo = $totalbloqueo + $respuesta['bloqueo'];
			if ($respuesta['accion'] == 'registro')
				$totalregistros = $totalregistros +1;

			if ($respuesta['accion'] == 'ingreso')
				$totalingresos = $totalingresos +1;

			if ($respuesta['accion'] == 'totalusuarioincorrecto')
				$totalusuarioincorrecto = $totalusuarioincorrecto +1;
		}


		if ($totalingresos > $maximoaingresoporip)
			$denegacion = 1;

		if ($totalregistros > $maximoregistroporip)
			$denegacion = 1;

		if ($totalusuarioincorrecto > $maximousuarioincorrectoporip)
			$denegacion = 1;


		// inserta el nuevo acceso
		$bdacceso->nuevo ();
		$bdacceso->valor ('ip'     , $ip);
		$bdacceso->valor ('fecha'  , $ahora);
		$bdacceso->valor ('archivo', $archivo);
		$bdacceso->valor ('accion' , $accion);
		$bdacceso->valor ('bloqueo', $denegacion);
		$bdacceso->insertar();


		if ($denegacion == 0)
			return false;
		else
			return true;
	}






function ContadorDeVisitas ($nombreArchivo = 'contador.txt') {

	if (file_exists($nombreArchivo))
		$archivo = fopen($nombreArchivo,'r+');
	else
		$archivo = fopen($nombreArchivo,'w+');
	flock($archivo, LOCK_EX);
	$valor = fgets($archivo);
	if ($valor == '')
	$valor = '0';
	rewind($archivo);
	fputs($archivo,++$valor);
	fclose($archivo);
	return $valor;
}






// OPERACIONES ----------------------------

// usuario y clave


	// si la palabra es válida, devuelve true, sino un mensaje de error_get_last
	function validar ($cadena, $mostrar = '**** (escondido)') {
		if ($mostrar == null)
			$mostrar = $cadena;

		if ($cadena != sanar(caracteres(sinespacio($cadena))))
			return ("La palabra $mostrar tiene caracteres no validos intente con números (0 - 9) y letras (a - z)");

		if (strlen($cadena) < 4)
			return ("La palabra $mostrar es muy corta, use una palabra de más de 4 caracteres y menos de 20");

		if (strlen($cadena) > 20)
			return ("La palabra $mostrar excede el limite, use una palabra de más de 4 caracteres y menos de 20");

		return true;
	}







	function ingresar ($usuario, $clave, $base) {

		if (ipbloqueada ('index', 'ingreso', $base ))
			return  texto('error conexiones');

		$respuesta = validar ($usuario, $usuario) ;
		if (is_string($respuesta))
			return $respuesta;

		$respuesta = validar ($clave, '**** (clave)') ;
		if (is_string($respuesta))
			return $respuesta;


		$clave = md5($clave);

		// USUARIO -----------------
		// busca en la base de datos si el usuario existe
		$bdusuarios = new BDDTabla ('usuarios', $base);
		$bdusuarios->valor  ('usuario', $usuario);
		$bdusuarios->valor  ('clave'  , $clave);
		if ($bdusuarios->existe() == false)
			return  texto('error usuario o clave incorrecto');

		return true;

	}













function publicidadamazon () {
   publicidad ('horizontal');
}


function publicidad ($posicion = 'horizontal') {
	if  (local())
		return 'Espacio de Publicidad';




	if ($posicion== 'adaptable')
		return "<script async src=\"//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js\"></script>
			<!-- adaptable -->
			<ins class=\"adsbygoogle\"
				 style=\"display:block\"
				 data-ad-client=\"ca-pub-8178043305036289\"
				 data-ad-slot=\"8877875993\"
				 data-ad-format=\"auto\"></ins>
			<script>
			(adsbygoogle = window.adsbygoogle || []).push({});
			</script>";




	if 	($posicion == 'vertical')
		return "<script type=\"text/javascript\">
			google_ad_client = \"ca-pub-8178043305036289\";
			width = document.documentElement.clientWidth;

			/* cartelito texto */
			 google_ad_slot = \"1022139534\";
			google_ad_width = 120;
			google_ad_height = 90;


			if (width > 900) {
				/* cartel vertical grande */
				google_ad_slot = \"2611850548\";
				google_ad_width = 300;
				google_ad_height = 1050;
			}

			</script>
			<!-- vertical300x1050 -->
			<script type=\"text/javascript\"
			src=\"//pagead2.googlesyndication.com/pagead/show_ads.js\">
			</script> ";





	if 	($posicion== 'horizontal')
		return "
			<script type=\"text/javascript\"><!--
			google_ad_client = \"ca-pub-8178043305036289\";
			width = document.documentElement.clientWidth;


			/* cartelito texto */
			 google_ad_slot = \"1022139534\";
			google_ad_width = 120;
			google_ad_height = 90;


			if (width > 300) {
			/* cartel chico */
				google_ad_slot = \"7287852458\";
				google_ad_width = 234;
				google_ad_height = 60;
			}



			if (width > 500) {
			/* cartel medio */
				google_ad_slot = \"1041895809\";
				google_ad_width = 468;
				google_ad_height = 60;
			}


			if (width > 1000) {
			/* cartel grande */
				google_ad_slot = \"4100617069\";
				google_ad_width = 970;
				google_ad_height = 250;
			}

			//-->
			</script>
			<script type=\"text/javascript\"
			src=\"http://pagead2.googlesyndication.com/pagead/show_ads.js\">
			</script> ";


			return '';

}












	function iconosvg () {
		echo "
<!-- Created with Inkscape (http://www.inkscape.org/) -->

<svg
   xmlns:dc='http://purl.org/dc/elements/1.1/'
   xmlns:cc='http://creativecommons.org/ns#'
   xmlns:rdf='http://www.w3.org/1999/02/22-rdf-syntax-ns#'
   xmlns:svg='http://www.w3.org/2000/svg'
   xmlns='http://www.w3.org/2000/svg'


   viewBox='0 0 199.99999 199.99999'
   >



   <ellipse
     style='fill:#448aff;fill-opacity:1'
     id='path3378'
     cx='99.999985'
     cy='100'
     rx='98.999985'
     ry='98.999977' />
  <path
     d='m 97.335969,170.00002 0,-14.5045 a 53.808221,53.808224 0 0 1 -47.78356,-47.83146 l -14.552413,0 0,-11.328043 14.504473,0 a 53.808221,53.808224 0 0 1 47.8315,-47.78354 l 0,-14.5525 11.328051,0 0,14.5045 a 53.808221,53.808224 0 0 1 47.78357,47.83154 l 14.55239,0 0,11.328043 -14.50446,0 a 53.808221,53.808224 0 0 1 -47.8315,47.78356 l 0,14.5524 -11.328051,0 z m 5.664011,-26.46381 a 41.53617,41.536172 0 0 0 41.53617,-41.53617 41.53617,41.536172 0 0 0 -41.53617,-41.536153 41.53617,41.536172 0 0 0 -41.536161,41.536153 41.53617,41.536172 0 0 0 41.536161,41.53617 z m 0,-18.50247 a 23.033694,23.033695 0 0 1 -23.033691,-23.0337 23.033694,23.033695 0 0 1 23.033691,-23.033693 23.033694,23.033695 0 0 1 23.03371,23.033693 23.033694,23.033695 0 0 1 -23.03371,23.0337 z'
     style='fill:#969ef7;fill-opacity:1' >
	 <animateTransform
				attributeName='transform'
				begin = '0s'
				dur='120s'
				type='rotate'
				from='0 103 102'
				to='90 103 102'
				fill='freeze'
				repeatCount='indefinite'
				/>

	</path>



    <path
       style='fill:#ffffff'
       d='m 94.33597,168.00002 0,-14.5045 A 53.808221,53.808224 0 0 1 46.55241,105.66406 l -14.552413,0 0,-11.328043 14.504473,0 a 53.808221,53.808224 0 0 1 47.8315,-47.7835 l 0,-14.5525 11.32805,0 0,14.5045 a 53.808221,53.808224 0 0 1 47.78357,47.8315 l 14.55239,0 0,11.328043 -14.50446,0 a 53.808221,53.808224 0 0 1 -47.8315,47.78356 l 0,14.5524 -11.32805,0 z m 5.66401,-26.46381 A 41.53617,41.536172 0 0 0 141.53615,100.00004 41.53617,41.536172 0 0 0 99.99998,58.463887 41.53617,41.536172 0 0 0 58.46382,100.00004 41.53617,41.536172 0 0 0 99.99998,141.53621 Z m 0,-18.50247 a 23.033694,23.033695 0 0 1 -23.03369,-23.0337 23.033694,23.033695 0 0 1 23.03369,-23.033693 23.033694,23.033695 0 0 1 23.03371,23.033693 23.033694,23.033695 0 0 1 -23.03371,23.0337 z'

        >
		<animateTransform
				attributeName='transform'
				begin = '0s'
				dur='120s'
				type='rotate'
				from='0 100 100'
				to='90 100 100'
				fill='freeze'
				repeatCount='indefinite'
				/>

	</path>



</svg>
";
	}



?>
