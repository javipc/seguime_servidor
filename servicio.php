<?php



	// si usas un bot de telegram, cambia a TRUE
	$habilitartelegram = true;



	require_once('base.php');

	define ('versionServidor', 4);


	echo "\r\n";




	// --------------------------------------------
	// SERVICIO
	// este archivo lo utiliza la aplicación móvil
	// --------------------------------------------




	if (local() == false)
		if ($_SERVER['HTTP_USER_AGENT'] != 'seguime 4') {
			mostrarestado ('Versión no compatible. ¡Visitá el sitio en internet y actualizá tu versión!');
			die();
		}















	// usuario --------------------------------

	// usuario viene del formulario enviado por la aplicación (o url) (no de una cookie de sesión)
	$usuario = dato('usuario', '');

	if ($usuario == '')
		die;





	// recibe comandos
	$comando   = dato('comando');
	$parametro = dato('parametro');


	// obtiene fecha y hora actual
	$ahora = ahora();


	// conecta con la base de datos
	$bd = conectar();

	if ($bd->desconectado()) {
		mostrarestado ( 'Parece que la base de datos no está funcionando');
		die('');
	}




	// PROTECCIÓN CONEXIÓN MASIVA ----------------------------------------------------------------------------

	// realiza una revision de la ip
	if ($comando == 'sesion')
		if (ipbloqueada ('servicio', 'ingreso', $bd )) {
			mostrarestado ('algo raro esta pasando con tu conexión, hay muchos intentos de iniciar sesión, por favor espera unas horas');
			die();
		};


	if ($comando == 'registro')
		if (ipbloqueada ('servicio', 'registro', $bd )) {
			mostrarestado ( 'algo raro esta pasando con tu conexión, hay muchos intentos de registros, por favor espera unas horas');
			die();
		};





	// ----------------------------------------------------------------------------

	// INICIO DE SESION Y REGISTROS -----------------
	// busca el usuario en la base de datos



	$usuario = minusculas($usuario);
	$clave = dato ('clave');


	$bdusuario = new BDDTabla ('usuarios', $bd);
	$bdusuario->valor('usuario', $usuario);

	// registra un usuario
	if ($comando == 'registro')
		if ($bdusuario->existe() == false) {
			$clave = md5($clave);
			$bdusuario->valor ('clave', $clave);
			$bdusuario->valor ('fecha', $ahora);
			$bdusuario->insertar ();

			mostrarsesion ('true');
			die();
		} else {
			mostrarestado ('el usuario ya existe');
			mostrarsesion ('false');
			die();
		}



	$acceso = ingresar ($usuario, $clave, $bd);

		if (is_string ($acceso))  {
			mostrarestado ($acceso);
			mostrarsesion ('false');
			die();
		}


	// envía comando a la aplicación para que permita la sesión
	if ($comando == 'sesion')
		if (is_bool($acceso))
			if ($acceso === true)
				mostrarsesion ('true');











	// ----------------------------------------------------------------------------







	// recibe información de ubicación para almacenar
	$latitud   = dato ('latitud'  , '' );
	$longitud  = dato ('longitud' , '' );
	$extra     = dato ('extra'    , '' );
	$proveedor = dato ('proveedor', '' );
	$codigo    = dato ('codigo'   , '' );
	$id        = dato ('id'       , '0');
	$velocidad = dato ('velocidad', '0');
	$fecha     = dato ('fecha'    , '2017-20-7 13:15');

	$imagen    = dato ('imagen'   , '' );






	// ingresa nuevas coordenadas a la base de datos
	if ($latitud != '' || $longitud != '') {

		$bdcoordenada = new BDDTabla ('coordenadas', $bd);
		$bdcoordenada->valor('usuario'  , $usuario)
					 ->valor('latitud'  , $latitud)
					 ->valor('longitud' , $longitud)
					 ->valor('velocidad', $velocidad)
					 ->valor('extra'    , $extra)
					 ->valor('fecha'    , $fecha);

		// si la coordenada no existe, la agrega
		if ($bdcoordenada->existe() == false)
			$bdcoordenada->insertar();

		// vuelve a consultar si existe (para evitar error)
		// visualiza el comando para que el dispositivo marque la coordenada como enviada
		if ($bdcoordenada->existe() == true)
			mostrarcomando ('marcar', $codigo);

	}




	// ingresa nuevas imágenes a la base de datos

	if ($imagen != '') {
		$verificacion = dato ('verificacion', -1);
		if (strlen($imagen) != $verificacion)
			$imagen = '';
	}



	if ($imagen != '') {

		$imagen = str_ireplace (' ', '+', $imagen);


		$bdimagen = new BDDTabla ('imagen', $bd);
		$bdimagen    ->valor('usuario'  , $usuario)
					 ->valor('imagen'   , $imagen)
					 ->valor('extra'    , $extra)
					 ->valor('fecha'    , $fecha);

		// si la imagen no existe, la agrega
		$bdimagen->insertarSiNoExiste();


		// vuelve a consultar si existe (para evitar error)
		if ($bdimagen->existe('usuario', 'fecha', 'extra'))
			mostrarcomando ('marcarimagen', $codigo);

	}








	// ----------------------------------------------------------------------------

	// visualiza los comandos del usuario pendientes en la base de datos

	$bdinstrucciones = new BDDTabla ('instrucciones', $bd);
	$bdinstrucciones->condicion  ('usuario', $usuario);
	$bdinstrucciones->seleccionar();

	while ($resultado = $bd->siguienteTupla())
		mostrarcomando ($resultado['comando'], $resultado['parametro']);

	// borra las instrucciones enviadas
	$bdinstrucciones->eliminar();








	// TELEGRAM ----------------------------------------------------------------------------

	if ($habilitartelegram == false)
		die();



	// telegram
	require_once('botseguime.php');

	// envia coordenadas a Telegram
	$telegram = dato('telegram', '');


	// identifica una coordenada que no es una posición actual
	$posicionNoActual = dato ('posicionHistorial', false);

	if ($telegram != '')
		if ($posicionNoActual == false)
			if ($latitud != '' || $longitud != '') {

				// envía coordenada
				enviarCoordenada ($telegram, $latitud, $longitud);
				enviarMensaje ($telegram, "Posición de: $usuario");
			}









	// ALERTAS (requiere telegram.php) ----------------------------------------------------------------------------

	require_once('alerta.php');
	alertaEjecutar();

	$alarma = dato('alarma', '');
	$texto = dato('texto');
	$texto = caracteres(tildes(utf8_encode($texto)));


	if($alarma != 'sin alarma')
		// borra la alarma
		if ($alarma == '0' || $alarma == '' || $alarma == 'borrar') {
			alertaBorrar ($usuario);
			alertaBorrarContactos($usuario);
			mostraralarma ('false');
		} else {
		// ingresa una alerta
			alertaContactoNuevo ($usuario, $telegram, $tipo='telegram');
			alertaNueva ($usuario, $alarma, $texto) ;
			mostraralarma ('true');
		}








	// esto es solo para desarrollo


	// visualiza formulario que imita comandos de la aplicacion
	$formulario = dato('formulario');


		// genera un formulario
		// para probar enviar comandos y el comportamiento de este programa
		$idFormulario = $id +1;
		if ($formulario != '')
			echo "
			<html>
				<head>
					<title>Seguime</title>
					<link rel='stylesheet' type='text/css' href='estilo.css' />
					<meta name=viewport content='width=device-width, initial-scale=1'/>
					<meta http-equiv='Content-Type' content='text/html;charset=utf8' />
				</head>

				<body>

					<div class=encabezado>
						<h1>Seguime</h1>
					</div>

				<div class=formulario>
					<form>
						latitud: <input type='text' name='latitud' value='36.45168264899959' placeholder='36.45168264899959' /> <br/>
						longitud: <input type='text' name='longitud' value='-103.18423748016357' placeholder='-103.18423748016357' /> <br/>
						fecha: <input type='text' name='fecha' value='$ahora' placeholder='2017-02-02 13:15' /> <br/>
						id: <input type='text' name='id' value='$idFormulario' /> <br/>

						<input type='submit'  value='Enviar' /><br/>
						<input type='hidden' name='formulario' value='formulario'/>

						<input type=hidden name=usuario value='$usuario' />
						<input type=hidden name=clave value='$clave' />
					</form>

				</div>
			</body></html>
			";







		$bd->cerrar();









	// saca por pantalla (web) un comando dirigido a la aplicacion
	function mostrarcomando ($comando, $parametro = '') {
		echo "$comando $parametro";
		echo "\r\n";
	}



	function mostrarnotificacion ($mensaje) {
		mostrarcomando ('notificacion', $mensaje) ;
	}

	function mostrarestado ($mensaje) {
		mostrarcomando ('mensajeestado', $mensaje) ;
	}

	function mostrarsesion ($estado) {
		mostrarcomando ('sesion', $estado) ;
	}

	function mostraralarma ($estado) {
		mostrarcomando ('alarmaservidor', $estado) ;
	}




?>
