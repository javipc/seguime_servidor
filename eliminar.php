<?php

	session_start();

	if (isset($_SESSION['usuario'])) 
		$usuario = $_SESSION['usuario'];
	else {
		header("Location: index.php?usuario=demo");
		die();
	}
	
	require_once("base.php");	
	require_once("faphp.php");
	
	
	$idioma = dato ('idioma', "");
	if ($idioma != "")
		setcookie("idioma",$idioma);
	
	
	
	$control = dato('control', "");
	
	
	$mensaje = dato ('mensaje', "");
	
	
	
	
	
	
	function eliminar ($usuario, $tabla, $bd) {
		$bdtabla = new BDDTabla ($tabla, $bd);
		$bdtabla->condicion ('usuario', $usuario);
		$bdtabla->eliminar();
	}
	
	
	
		
	
	
	
	
	if ($control=="eliminardatos" || $control=="eliminarcuenta") {
		
		$bd = conectar();
		
		
		eliminar ($usuario, 'alertas', $bd);
		eliminar ($usuario, 'contactos', $bd);
		eliminar ($usuario, 'coordenadas', $bd);
		eliminar ($usuario, 'instrucciones', $bd);
		eliminar ($usuario, 'contactos', $bd);
		eliminar ($usuario, 'imagen', $bd);		
		
	}
	
	
	
	
	
	
		
	
	// elimina la cuenta
	if ($control=="eliminarcuenta") {
			
		// elimina el usuario
		eliminar ($usuario, 'usuarios', $bd);
		
		session_destroy();
		header("Location: index.php");
		die();
	}
	
	if ($control=="eliminardatos") {
		header("Location: panel.php");
		die();
	}
	
	
			
	?>
<!DOCTYPE html>
<html class="noIE" lang="es-ES">
<!--<![endif]-->
	<head>
		<title>Seguime</title>

		<!-- meta -->
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
		<META NAME='Author' CONTENT='Javier' />
		<META NAME='Generator' CONTENT='notepad++' />
		<META NAME='Language' CONTENT='Spanish' />
		<meta name='language' content='Español' />
		<META NAME='DC.Language' CONTENT='Spanish' />
		<META NAME='Revisit' CONTENT='5 day' />
		<meta name='revisit-after' content='5 days' />
		<META NAME='Distribution' CONTENT='Global' />
		<meta name='document-distribution' content='Global' />
		<META NAME='Robots' CONTENT='All' />
		<meta name='document-classification' content='aplication' />
		<meta name='document-rights' content='Public' />
		<meta name='document-type' content='Public' />
		<meta name='document-rating' content='General' />
		<meta name='document-state' content='Dynamic' />
		<meta name='cache-control' content='no-cache' />
		<META NAME='copyright' CONTENT='(c) 2017 - 2018' />
		<META NAME='DateCreated' CONTENT='dec 2017' />
		<META NAME='creacion' CONTENT='Diciembre 2017' />

		
		<meta name="viewport" content="width=device-width, initial-scale = 1.0, maximum-scale=1.0, user-scalable=no" />
		

		<link rel="shortcut icon" href="icono.png" />
		
		<!-- css -->

		
		<link rel="stylesheet" type="text/css" media="screen" href="esquema.css" />
		<link rel="stylesheet" type="text/css" media="screen" href="estilo.css"  />
		<link rel="stylesheet" type="text/css" media="screen" href="movil.css"   />
		
		

	</head>
	
	<body>
	
	
	<div class="publicidad centrado">
		<?php  echo publicidad(); ?>
	</div>

	
			
			
		<div  class="contenido ">
		
		
		
			<div class="bloque">
						<header class="centrado">
						
							<h3><?php echo texto("Eliminar"); ?></h3>
	
									<?php 
											if ($mensaje != "")
												echo "<div class=mensaje>$mensaje</div>";
											
										?>
						</header>
							
				<div class="tabla centrado">
					<div class=" fila">
						<div class="columna ">
							<p class="centrado ocultable">
								<?php svg ("eraser", 'red');  ?>
							</p>
						</div>
					
						<div class="columna izquierda">
						
						
						¿Que datos se eliminaran?
				<ul>
				
					<li> Tu nombre: Seguime ¡NO SABE TU NOMBRE!</li>
					<li> Tu correo electrónico: aquí nadie recopila datos de ese tipo, no se necesita ;-)</li>
					<li> Tu usuario: sí.</li>
					<li> Tu clave: por supuesto.</li>
					<li> Tus coordenadas: ¡TODAS!.</li>
					<li> Tu alarma: no debería haber, pero si la hay SÍ.</li>
					<li> Tu contacto de Telegram: no debería haber, pero si hay, entonces SÍ.</li>
					<li> Tus fotografías.
					<li> Tus IPs: para evitar abusos se almacena ip al registrar o al ingresar en cuentas, pero no están asociadas A NINGÚN USUARIO, por lo tanto no se sabe a quienes pertenecen, entonces no se pueden eliminar. Cabe aclarar que periódicamente se limpia la base de datos para borrar esa información.</li>
				</ul>
				<p>Para borrar datos de la aplicación, cierra la sesión y se eliminará toda la información.</p>
				<p>¡Muchas gracias por probar la aplicación! :)</p>
				
				
				
				
				
				
				<div class= "bloque centrado">
					<a href="eliminar.php?control=eliminarcuenta" class="boton redondeado rojo" ><?php echo texto("Eliminar Cuenta"); ?></a> 
					
					<a href="eliminar.php?control=eliminardatos" class="boton redondeado rojo" ><?php echo texto("Eliminar Datos"); ?></a> 
				</div>
				
						
								
								
								
						</div>
						
						
						
							
						
						
						
					</div>
				</div>
				
				
				
			</div>
			
			
			<div class= "bloque centrado">
				<a href="panel.php" class="boton redondeado verde" ><?php echo texto("volver"); ?></a>
			</div>
			
			
			
			

			
			
			<div class="publicidad centrado">
				<?php  echo publicidad("adaptable"); ?>
			</div>
		</div>
			

			
	</body>
</html>
