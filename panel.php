<?php
	
	
	session_start();


	if (isset($_SESSION['usuario'])) 
		$usuario = $_SESSION['usuario'];
	else {
		header('Location: index.php?usuario=demo');
		die();
	}
	
	
	require_once('base.php');
	require_once('faphp.php');
	
	
			
	/*
	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
	*/
		
			
	

	
	
	
	
		
		
	// --------------------------------------------------
	// WEB
	// este archivo lo utiliza el usuario como sitio web
	// --------------------------------------------------

			
	

	$mensaje ='';


	// para desarrollo - visualiza formulario que imita comandos de la aplicación
	$formulario = dato('formulario');
	
	// recibe comandos 
	$comando    = dato('comando');
	$parametro  = dato('parametro');
	
	// recibe información de ubicación para visualizar en el mapa
	$latitud    = dato('latitud', '');
	$longitud   = dato('longitud', '');
	$fecha      = dato('fecha', '2017-20-7 13:15');
	$id         = dato('id', '0');
	
	// uso interno
	$control    = dato('control');
	
	
	
	// conecta con la base de datos
	$bd = conectar();
	
	
		
	
	
		// ultima coordenada
		$bdcoordenadas = new BDDTabla ('coordenadas', $bd);
		
		$bdcoordenadas->condicion  ('usuario', $usuario);
		$bdcoordenadas->orden      ('fecha'  , 'desc');		
		$bdcoordenadas->limite     (1);
		$bdcoordenadas->seleccionar();
		
		$ultimaCoordenada = null;
		$respuesta = $bd->siguienteTupla();
		if (isset ($respuesta['latitud']))
			$ultimaCoordenada = $respuesta;
		
		
	








	
	// servicio (utilizado por javascript para obtener coordenadas nuevas sin recargar el php)
	if ($control == 'servicio') {
			
			$bdpanel = new BDDTabla ('panel', $bd);
			$bdpanel->condicion('usuario', $usuario);
			$bdpanel->seleccionar();
			

			$fecha = null;
			$reg = $bd->siguienteTupla();
			if (isset ($reg['visto']))
				$fecha = $reg['visto'];
							
			
			$bdcoordenadas = new BDDTabla ('coordenadas', $bd);
			$bdcoordenadas->condicion('usuario', $usuario);
			$bdcoordenadas->orden('fecha', 'asc');
			$bdcoordenadas->limite (100);
			if ($fecha != null)
				$bdcoordenadas->condicion('fecha', '>', $fecha);
						
			$bdcoordenadas->seleccionar('fecha', 'latitud', 'longitud', 'extra');
			$respuesta = array();
			while($reg = $bd->siguienteTupla()) 
				$respuesta[] = $reg;

			
			
			// actualiza la fecha de la ultima coordenada vista
			$bdcoordenadas->orden('fecha', 'desc');
			$bdcoordenadas->limite (1);
			$bdcoordenadas->seleccionar('fecha');
			$reg = $bd->siguienteTupla();
				
			
			if (isset ($reg['fecha'])) {
				$bdpanel = new BDDTabla ('panel', $bd);
				$bdpanel->condicion('usuario', $usuario);				
				$bdpanel->valor ('visto', $reg['fecha']);
				$bdpanel->actualizarOInsertar ();
			}
			
			
			echo json_encode($respuesta);
		die();
	}





	
			
			
			



	
	
		
		
	// cierra la sesion
	if ($control=='cerrarsesion') {
			session_destroy();
			header('Location: index.php');
			die();
	}
	
	
	
	
		
		
	// borra coordenadas
	if ($control=='borrarcoordenada') {
		// verifica que la coordenada corresponde con el usuario
	}
	
	
	// configuración opciones
	if ($control == 'opciones') 		
		if ($comando == 'actualizar') {
				$bdpanel = new BDDTabla ('panel', $bd);
				$bdpanel->condicion('usuario', $usuario);
				$bdpanel->valor ( 'refrescar', $parametro);
				$bdpanel->actualizarOInsertar ();
				if ($parametro > 0)
					$mensaje = "Se actualizará cada $parametro minutos";
				else
					$mensaje = "Actualización autom&aacute;tica deshabilitada";				
					
			}
		
	
	
	
	
	
	
	// configuración remota
	if ($control == 'remoto') {
		$comando = dato('comando', '');
		
		$bdinstruccion = new BDDTabla ('instrucciones', $bd);
		$bdinstruccion->valor ('usuario', $usuario);
		$bdinstruccion->valor ('comando', $comando);
		
		switch ($comando) {
			case 'actividad'  :
			case 'inactividad' :
			case 'sms':
			case 'telegram':
				$parametro = dato('parametro', '60');				
								
				$bdinstruccion->valor ('parametro', $parametro);
				$bdinstruccion->insertarSiNoExiste ();				
				$mensaje = 'comando listo para ser enviado';
				
			break;
			
			
			case 'rastreo':
			case 'bloqueo':				
				if (isset($_POST['parametro']))
					$parametro = 'true';
				else
					$parametro = 'false';
				
				$bdinstruccion->valor ('parametro', $parametro);
				$bdinstruccion->insertarSiNoExiste ();
				$mensaje = 'comando listo para ser enviado';
				
				break;
			
		}
		
	}

	
	
	
	
	
	
	
	
	
	
	

	
	
	

			// visualiza la pagina
?>
		
<html>
	<head>
		<title>Seguime</title>
		<link rel="stylesheet" type="text/css" media="screen" href="esquema.css" />
		<link rel="stylesheet" type="text/css" media="screen" href="panel.css"  />
		<link rel="stylesheet" type="text/css" media="screen" href="movil.css"   />
		
		<meta name=viewport content="width=device-width, initial-scale=1" />
		<meta http-equiv="Content-Type" content="text/html;charset=utf8" />
		<link rel="shortcut icon" href="icono.png">
		
		
		<script src="leaflet/leaflet.js	"></script>
		<link rel="stylesheet" href="leaflet/leaflet.css" />
		
		
		<script type="text/javascript">
			
			
			
			// se conecta con el servicio
			function ws () {
				
				document.getElementsByClassName('rotable')[0].classList.add('rotar');
				
				var url = "panel.php?control=servicio";
				
				
			console.log("contactando con " +url);
			
			
				var conexion = new XMLHttpRequest();
				//Dirección a la que accedemos de forma asíncrona a por los datos
				conexion.onload = function (e) {
					
					if (conexion.readyState === 4) {
					
						// Revisamos si la obtención de los datos fue satisfactoria
						if (conexion.status === 200) {
							console.log('respuesta');
							console.log(conexion.responseText);
							
							// envía la respuesta a una función que procesa los datos
							procesarRespuesta (conexion.responseText);
						} else {
							console.log("Error");
							console.error(conexion.statusText);
						}
					}
					conexion.abort;
					document.getElementsByClassName('rotable')[0].classList.remove('rotar');
				};
				
				conexion.open('GET', url, false);
				conexion.send(null);
			}
			
			
			
			
			// la respuesta del servicio se procesa aquí
			// extrae la información que proviene de la base de datos
			function procesarRespuesta (respuesta) {				
				var json = JSON.parse (respuesta);
				
				var latitud  = 0;
				var longitud = 0;
				if (json.length > 0)
					for (var x = 0; x < json.length; x++) {
						 latitud = json[x].latitud;
						longitud = json[x].longitud;
						   fecha = json[x].fecha;
						   extra = json[x].extra;
						nuevaCoordenada (fecha, latitud, longitud, extra);
					}
						
			}	
			
			
			
			
			// modifica el mapa | versión IFRAME
			
			function mapaiframe (latitud, longitud) {
				var mapa = document.getElementsByTagName ('iframe')[0];
				mapa.setAttribute('src',
				"http://www.openstreetmap.org/export/embed.html?"
							+ "bbox=" + longitud
							+ "%2C"   +  latitud
							+ "%2C"   + longitud
							+ "%2C"   +  latitud
							+ "&"
							+ "layer=mapnik"
							+ "&"
							+ "marker=" +  latitud
							+ "%2C"     + longitud
							+ " ");
			}
			
			
			
			
			// crea una nueva fila (etiqueta TR) con elementos dados
			function crearFila (...elementos) {
				// document.createTextNode('asi es');
				
				var tr = document.createElement ('tr');
				
				if (elementos.length > 0)
					for (var x = 0; x < elementos.length; x++) {
						var td = document.createElement ('td');
						td.append (elementos[x]);
						tr.append (td);
					}
				return tr;
			}
			
			
			
			// recibe datos para generar una nueva coordenada en el panel			
			function nuevaCoordenada (fecha, latitud, longitud, extra) {
				
				// actualiza el mapa
				movermapa (latitud, longitud);
				
				// crea la etiqueta enlace para la tabla
				var enlace = document.createElement('a');
				//$enlaceCoordenada =  "concat ('<a href=\'panel.php?latitud=', latitud, '&longitud=', longitud   , '\'  class = \'tablaEnlace\' >', latitud, '|' , longitud,'</a>') as Posicion";
				enlace.setAttribute ('class', 'tablaEnlace');
				enlace.setAttribute ('href', 'panel.php?latitud=' + latitud + '&longitud=' + longitud);
				enlace.setAttribute ('meta-latitud', latitud);
				enlace.setAttribute ('meta-longitud', longitud);
				enlace.appendChild (document.createTextNode ('(Nuevo) ' + latitud + ' ' + longitud));
				
				
				var fila = document.getElementsByTagName ('tr')[0];
				
				
				var tr = crearFila (
					document.createTextNode (fecha),
					enlace,
					document.createTextNode (extra)
					);
				
				fila.after(tr);
				modificarEnlaces();
				
				// efecto aparición
				tr.setAttribute ('style', " transform: rotatex(-90deg); height:0px ; opacity: 0; background-color: green");	
					window.setTimeout(function () {
						tr.setAttribute ('style', "-o-transition: all 5s; -ms-transition: all 5s; -moz-transition: all 5s; -webkit-transition: all 5s; transition: all 5s; background-color: #ff9");
					}, 1);
				
				
				// efecto color				
					window.setTimeout(function () {
						tr.setAttribute ('style', "-o-transition: all 60s; -ms-transition: all 60s; -moz-transition: all 60s; -webkit-transition: all 60s; transition: all 60s;");
					}, 10000);
			}
			
			
			
			
			
			
			// hace que los enlaces alteren el mapa, en vez de recargar la página
			function modificarEnlaces () {
				var tabla = document.getElementsByClassName('table')[0];
				var enlaces = tabla.getElementsByTagName('a');
				
				if (enlaces.length > 0)
					for (var x = 0; x < enlaces.length; x++) {
						if (enlaces[x].getAttribute ('href') == '#')
							continue;
						enlaces[x].setAttribute ('href', '#');
						let latitud  = enlaces[x].getAttribute ('meta-latitud');
						let longitud = enlaces[x].getAttribute ('meta-longitud');
						enlaces[x].addEventListener ('click', function (ev) { 
							movermapa (latitud, longitud); 
							ev.preventDefault();  
							console.log ('yendo a: ' + latitud + ' - ' + longitud);
							
						} );

					}
				
			}
			
			
			
			
			
		</script>
		
	</head>
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	<body>
			
		<div class='encabezado' >
			
				
				<span class=icono>
				<?php iconosvg(); ?>
				</span>
			<span>
				<h1>Seguime</h1>
			</span>	
				
			<span>
				<a href='panel.php'> 
				<span  class= 'rotable'>
				<?php 
					$imagen = new faphp ();
					$imagen->altura('30px');
					$imagen->color('#fff');
					$imagen->mostrar ('sync-alt');
				?>
				</span>
				</a>
			</span>
			
			<span>
				<a href='imagenes.php' > 
				<?php 					
					$imagen->mostrar ('camera-retro');
				?>
				</a>
			</span>
			
			<span>
				<a href='opciones.php' > 
				<?php 					
					$imagen->mostrar ('cog');
				?>
				</a>
			</span>
			
			
			
			<span>
				<a href='panel.php?control=cerrarsesion' class = 'rojofuerte'>
				<?php 					
					$imagen->mostrar ('times');
				?>
				</a>
			</span>
			
			
			
		</div>
		
	










	
		<?php 
			if($mensaje != '')
				echo "<div class = 'cartel centrado'>
							<p class=mensaje>$mensaje</p>
						</div>";
			?>
		
		












		
		<div class= "tabla">
			<div class="fila">
				<div class="columna">
		
					<div class="mapa" id="mapa"></div>

			
						
					
					
					
			</div>
				
			<div class="columna">
			
	<?php		



				


				
				
				
							
				
				// si no hay posición para mostrar, muestra la última coordenada de la base de datos
				if ($latitud == '' || $longitud == '')
					if ($ultimaCoordenada != null) {
						$latitud  = $ultimaCoordenada ['latitud' ];				
						$longitud = $ultimaCoordenada ['longitud'];
					}
				
					
				// mueve el marcador 	
				if ($latitud != '' && $longitud != '') 
					echo "<script type='text/javascript'> movermapa ($latitud, $longitud); </script>";
				
				

				$etiqueta = new BDDEtiqueta() ;
				$etiqueta->atributo     ('href');
				$etiqueta->literal      ('panel.php');	
				$etiqueta->literal      ('?latitud=');
				$etiqueta->columna      ('latitud');
				$etiqueta->literal      ('&longitud=');
				$etiqueta->columna      ('longitud');	
				$etiqueta->finAtributo  ();
				
				// para javascript
				
				$etiqueta->atributo     ('meta-latitud');			
				$etiqueta->columna      ('latitud');
				$etiqueta->finAtributo  ();
				
				$etiqueta->atributo     ('meta-longitud');
				$etiqueta->columna      ('longitud');
				$etiqueta->finAtributo  ();
				
						
				$etiqueta->contenido    ();
				$etiqueta->columna      ('latitud');
				$etiqueta->literal      (' ');
				$etiqueta->columna      ('longitud');	
				$etiqueta->finContenido ();
							
				$enlaceCoordenada =  'concat (' . $etiqueta->obtener('a') . ') as Posición';
				
				//$enlaceCoordenada =  "concat ('<a href=\'panel.php?latitud=', latitud, '&longitud=', longitud   , '\'  class = \'tablaEnlace\' >', latitud, '|' , longitud,'</a>') as Posicion";
				
				
				
				// consulta de coordenadas
				$bdcoordenadas = new BDDTabla ('coordenadas', $bd);
				$bdcoordenadas->condicion('usuario', $usuario);
				$bdcoordenadas->orden('fecha', 'desc');
				$bdcoordenadas->limite(500);
				$bdcoordenadas->seleccionar('fecha as Fecha', $enlaceCoordenada, 'extra as Extra');
				
				echo '<div class=table>';
				$bd->imprimir("border = '0'");
				echo '</div>';
				
				


		// actualiza la base de datos para mostrar solo las nuevas coordenadas				
			if ($ultimaCoordenada != null) {
				$bdpanel = new BDDTabla ('panel', $bd);
				$bdpanel->condicion('usuario', $usuario);
				$bdpanel->valor ('visto', $ultimaCoordenada['fecha']);				
				$bdpanel->actualizarOInsertar ();				 
			}



				
		
			
				
				
		
		








	

	
	
?>
		

		</div>
		</div>
		</div>
		
			
	
<footer>
	<p> Actualización: 16 Julio 2019 </p>
</footer>


<?php
			
			$bdpanel = new BDDTabla ('panel', $bd);
			$bdpanel->condicion('usuario', $usuario);
			
			$bdpanel->seleccionar();
			

			$refresco = 0;
			$reg = $bd->siguienteTupla();
			if (isset ($reg['refrescar']))
				$refresco = $reg['refrescar'];
			
			
		if ($refresco > 0)
			echo "  <script type='text/javascript'>
						var temporizador = setInterval (function () {ws();} , (60 * 1000 * $refresco ))
					</script>";
					
					
					
	$bd->cerrar();
?>


<script type='text/javascript'>
	modificarEnlaces ();
</script>





<script type="text/javascript">
							// Muestra el mapa
							
							var mapa = L.map('mapa');


							var OSM_Mapnik = L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
								maxZoom: 19,
								attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
							}).addTo(mapa);

							
							mapa.setView([-33, -66], 13);

							var marker = L.marker([-33, -66]).addTo(mapa);


							function movermapa (lat, lon) {
								var newLatLng = new L.LatLng(lat, lon);     
								marker.setLatLng(newLatLng); 
								mapa.setView([lat, lon]);
							}
					</script>

	</body>
</html>
			
		

	
