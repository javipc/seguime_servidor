<?php
	session_start();
	require_once('base.php');
	require_once('faphp.php');
	
	
	

	// -----------------------------------
	// pagina principal
	// -----------------------------------
	$idioma = dato ('idioma', '');
	if ($idioma != '')
		setcookie('idioma',$idioma);
	
	
	
	$control    = dato ('control'   , '');
	$usuario    = dato ('usuario'   , '');
	$clave      = dato ('clave'     , '');	
	$mensaje    = dato ('mensaje'   , '');
	$publicidad = false;
	$usuario    = minusculas($usuario);
	
	
	
	
	
	// conecta con la base de datos
	$bd = conectar();
	if ($bd->desconectado()) {
		$mensaje =  texto ('error bdd');
		$control = '';
		$usuario = '';
		
	}
	
	
	
	if ($control == 'ingresar') {
		$acceso = ingresar ($usuario, $clave, $bd);
		
		if (is_bool($acceso)) 
			if ($acceso === true) {				
				$_SESSION['usuario'] = $usuario;
				header('Location: panel.php');
				die();				
			}
				
				
		if (is_string ($acceso)) 
			$mensaje = $acceso;
	}
		
	$bd->liberar();
	$bd->cerrar ();
		
			
			
		
			
	?>

<!DOCTYPE html>

<!--[if IE 7 ]><html class="ie ie7 lte9 lte8 lte7" lang="en-US"><![endif]-->
<!--[if IE 8]><html class="ie ie8 lte9 lte8" lang="en-US">	<![endif]-->
<!--[if IE 9]><html class="ie ie9 lte9" lang="en-US"><![endif]-->
<!--[if (gt IE 9)|!(IE)]><!-->
<html class="noIE" lang="es-ES">
<!--<![endif]-->
	<head>
		<title>Seguime</title>

		

		<link rel="shortcut icon" href="icono.png" />
		
		<!-- css -->

		
		<link rel="stylesheet" type="text/css" media="screen" href="esquema.css" />	
		<link rel="stylesheet" type="text/css" media="screen" href="estilo.css"  />
		<link rel="stylesheet" type="text/css" media="screen" href="movil.css"   />

		
		
		

		<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!--[if lt IE 9]>
			<script src="recursos/js/html5shiv.js"></script>
			<script src="recursos/js/respond.js"></script>
		<![endif]-->

		<!--[if IE 8]>
	    	<script src="recursos/js/selectivizr.js"></script>
	    <![endif]-->
	</head>
	
	<body>
	
	
					<section class="encabezado encabezadofijo">
						<span class='icono'>
							<?php iconosvg(); ?>
						</span>
							
							<h1>  Seguime </h1> 
							
						
						
					</section>
	
	
	
			
			<!-- sesion -->
			
			
			
		<div id="sesionid" class="contenido sesion">
		
			<div class="bloque">
						<header class="centrado">
							<h3><?php echo texto("iniciar sesion"); ?></h3>
									<?php 
											if ($mensaje != "")
												echo "<div class='mensaje rojo'>$mensaje</div>";
											
										?>
						</header>
							
							
						<div class="tabla">
							<div class=" fila">
								<div class="columna ocultable">
									<p class="centrado ">
										<?php svg ("user", '#fff');  ?>
									</p>
								</div>
							
								<div class="columna">
										<form method="post" action="index.php#sesionid" class="formulario centrado">
											
												<input type="text" placeholder="<?php echo texto("tu usuario"); ?>" required="" maxlength="15" name="usuario"> <br/>
												<input type="password" placeholder="<?php echo texto("tu clave"); ?>" required="" maxlength="15" name="clave"> <br/>
											
											
										  <input type="submit" value="<?php echo texto("iniciar sesion"); ?>" class="boton redondeado verde">
										  <input type="hidden" name="control" value="ingresar">
										</form>
								</div>
								
							</div>
						</div>
							
			</div>
		</div>
			<!-- #sesion -->
			
			
			
			
			
					
			
			
			
			
						
			
			
			<div class="contenido donacion" id="donacionid">
				<div class="  centrado">
					<header class="bloque">
						<h3><?php echo texto("apoya a seguime"); ?></h3>
						<p><?php echo texto("para que siga libre"); ?></p>
					</header>
					
					
					
					<section class="bloque">
						<div class= "tablaflexible">
							<div class="fila">
							
								<div class="columna ocultable">
									
										<?php svg ("coffee", '#621');  ?>
									<p class = "frase"><?php echo texto("invita un cafe"); ?></p>
								</div>
								
								<div class="columna">
									<p>
										<?php echo texto("manito extendida"); ?>
									</p>
								
								<p>	

									<a href="http://javim.000webhostapp.com/donacion" class="enlace" >HACER UNA DONACI&Oacute;N</a>
								</p>
								</div>
								
								
								
							</div>
						</div>
					</section>
				</div>
			</div>
			

			
			
			
			
		


			




			
			





		
		
		


		
		
		

	</body>
</html>
