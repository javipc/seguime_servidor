<?php

	
	/*
	
		BOT Telegram | Javier 2019
		
		enviarMensaje (id, texto);
		
		enviarRespuesta (id, id_mensaje, texto);
		
		enviarCoordenada (id, latitud, longitud);
		
		enviarDato (id, comando, parametros[]);

	
		
		
		Los mensajes recibidos los envia a una función que debe escribirse
		-> procesarMensaje ($chat_id, $mensaje_id, $nombre, $texto);
		
		
			require_once("bot.php");
				
			define('bot_clave', 'clave');	
			define('bot_url', 'https://api.telegram.org/bot'.bot_clave.'/');	
			define('WEBHOOK_URL', 'https://url..../tu_telegrambot.php');
	
			function procesarMensaje ($chat_id, $mensaje_id, $nombre, $texto) { }

		
			// funcion adicional que guarda el numero de actualizacion
			function guardarNumeroActualizacion($numero) { }
		
		
		
	*/



	
	
	// WH --------------------------------------------------------

			
	function apiRequestWebhook($method, $parameters) {
	  if (!is_string($method)) {
		error_log ("Method name must be a string\n");
		return false;
	  }

	  if (!$parameters) {
		$parameters = array();
	  } else if (!is_array($parameters)) {
		error_log("Parameters must be an array\n");
		return false;
	  }

	  $parameters["method"] = $method;

	  header("Content-Type: application/json");
	  echo json_encode($parameters);
	  return true;
	}


	function apiWebhook($parameters) {
	  header("Content-Type: application/json");
	  echo json_encode($parameters);
	  return true;
	}

	//  --------------------------------------------------------









	// ------ CONEXION -------------------
		
	// función que realiza la conexión con el bot
	function conectarURL ($parametros = array('method' => "getUpdates")) {
				
		// verifica si son arreglos
		if (!is_array($parametros)) {
			error_log("Los parametros tienen que ser un arreglo \n");
			return false;
		}
				
		// verifica que se envíe un comando valido
		$comando = $parametros['method'];
		if (!is_string($comando)) {
			error_log("El comando (method) debe ser una cadena \n");
			return false;
		}
		
		// prepara la conexión
		$handle = curl_init(bot_url);
		curl_setopt($handle, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($handle, CURLOPT_CONNECTTIMEOUT, 25);
		curl_setopt($handle, CURLOPT_TIMEOUT, 60);
		curl_setopt($handle, CURLOPT_POSTFIELDS, json_encode($parametros));
		curl_setopt($handle, CURLOPT_HTTPHEADER, array("Content-Type: application/json"));
		
		// realiza la conexión		
		$respuesta = curl_exec ($handle);
		
		curl_close($handle);
		return $respuesta;
	}
































	// ------ ENVIO DE MENSAJES WH -------------------
	function enviarMensajeWH ($destinatario, $mensaje, $id_respuesta="") {
		$parametro = Mensaje($destinatario, $mensaje, $id_respuesta="");		
		return enviarWH ($parametro);
	}
	
	function enviarCoordenadaWH ($destinatario, $latitud, $longitud, $id_respuesta="") {
		$parametro = Coordenada ($destinatario, $latitud, $longitud, $id_respuesta="");
		return enviarWH ($parametro);
	}

	function enviarDatoWH ($destinatario, $comando, $parametro = array()) {
		$parametro = OtroDato ($destinatario, $comando, $parametro);
		return enviarWH ($parametro);
	}
	
		
	function enviarWH ($parametro = array()) {
		return apiWebhook ($parametro);
	}
	
	
	
	// ------ ENVIO DE MENSAJES API -------------------
	function enviarMensajeAPI ($destinatario, $mensaje, $id_respuesta="") {
		$parametro = Mensaje($destinatario, $mensaje, $id_respuesta="");		
		return enviarAPI ($parametro);
	}
	
	function enviarCoordenadaAPI ($destinatario, $latitud, $longitud, $id_respuesta="") {
		$parametro = Coordenada ($destinatario, $latitud, $longitud, $id_respuesta="");
		return enviarAPI ($parametro);
	}

	function enviarDatoAPI ($destinatario, $comando, $parametro = array()) {
		$parametro = OtroDato ($destinatario, $comando, $parametro);
		return enviarAPI ($parametro);
	}
	
		
	function enviarAPI ($parametro = array()) {
		$respuesta = conectarURL ($parametro);
		$jsonRespuesta = json_decode($respuesta, true);
		return comprobarRespuesta($jsonRespuesta);
	}
	



	// ------ ENVIO DE MENSAJES (compatible) -------------------	
	
	function enviarMensaje ($destinatario, $mensaje, $id_respuesta="") {
		$parametro = Mensaje($destinatario, $mensaje, $id_respuesta="");		
		return enviar ($parametro);
	}
	
	function enviarCoordenada ($destinatario, $latitud, $longitud, $id_respuesta="") {
		$parametro = Coordenada ($destinatario, $latitud, $longitud, $id_respuesta="");
		return enviar ($parametro);
	}

	function enviarDato ($destinatario, $comando, $parametro = array()) {
		$parametro = OtroDato ($destinatario, $comando, $parametro);
		return enviar ($parametro);
	}
	
		
	global $modoWH;	
	$modoWH = false;
	function enviar ($parametro = array()) {
		global $modoWH;
		if ($modoWH)
			return enviarWH($parametro);
		else
			return enviarAPI($parametro);
	}
	









	// obtiene un arreglo de parametros listo para ser enviado, según el tipo de mensaje ----------------

	function Mensaje ($destinatario, $mensaje, $id_respuesta="") {
		$parametro = parametroDestinatario ($destinatario);
		$parametro = parametroMensaje ($mensaje, $parametro);
		if ($id_respuesta != "")
			$parametro = parametroRespuesta ($id_respuesta, $parametro);
		return $parametro;
	}
		
		
	function Coordenada ($destinatario, $latitud, $longitud, $id_respuesta="") {
		$parametro = parametroDestinatario ($destinatario);				
		$parametro = parametroCoordenada ($latitud, $longitud, $parametro);
		if ($id_respuesta != "")
			$parametro = parametroRespuesta ($id_respuesta, $parametro);
		return $parametro;
	}

	function OtroDato ($destinatario, $comando, $parametro = array()) {
		$parametro = parametroDestinatario ($destinatario, $parametro);
		$parametro = parametroComando ($comando, $parametro);
		return $parametro;
	}
	








	
	
	
	
	// ---------- prepara el arreglo de envios ---------------------------------	
	
	// usuario destinatario
	function parametroDestinatario ($chat_id, $parametro = array()) {				
		$formato="html";		
		$parametro['chat_id'] = $chat_id;
		$parametro['parse_mode']=$formato;
		return $parametro;
	}
	
	// mensajes de texto
	function parametroMensaje ($mensaje, $parametro=array()) {
		$parametro['method'] = "sendMessage";
		$parametro['text']=$mensaje;
		return $parametro;
	}
	
	// respuesta a mensajes
	function parametroRespuesta ($id_mensaje, $parametro = array()) {		
		$parametro['reply_to_message_id']=$id_mensaje;
		return $parametro;
	}
	
	// coordenadas
	function parametroCoordenada ($latitud, $longitud, $parametro = array()) {
		$parametro['method'] = "sendLocation";		
		$parametro['latitude']=$latitud;
		$parametro['longitude']=$longitud;
		//$parametro['live_period']="700";		
		return $parametro;
	}
	
	// otros datos
	function parametroComando ($comando, $parametro= array()) {
		$parametro['method'] = $comando;
		return $parametro;
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	function recibirMensaje ($message) {		
		$mensaje_id = $message['message_id'];
		$chat_id = $message['chat']['id'];
		$nombre = $message['from']['first_name'];
		$texto = "";
		if (isset($message['text']))
			$texto = $message['text'];
		
		procesarMensaje ($chat_id, $mensaje_id, $nombre, $texto);
	}
	
	
	

	
	
	
	
	
	
	
	
	
	





	$content = file_get_contents("php://input");
	$update = json_decode($content, true);

	if ($update)
		if (isset($update["message"])) {
			global $modoWH;
			$modoWH = true;
			recibirMensaje ($update["message"]);	  	  			
		}
		else {
			return comprobarRespuesta ($update);
		}
	
	
	function comprobarRespuesta ($arregloRespuesta) {		
			if (isset($arregloRespuesta["ok"])) 
				return $arregloRespuesta['ok'];
	}
	
	
	// METODO API http	| en desarrollo 
	
	// función que obtiene mensajes nuevos del bot | método api
	function obtenerActualizacion($actualizacion = 0) {
		$parametro = array();				
		$parametro['method'] = "getUpdates";
		$parametro['offset']  = $actualizacion;
		
		$respuesta = conectarURL ($parametro);
						 
		return $respuesta;
	}
	
	
	// funcion que verifica si hay mensajes del bot para recibir
	function verificarMensaje($contenido) {		
		$arreglo = json_decode($contenido, true);
		
		  // esto no debería ocurrir
		if (!$arreglo) 
		  return;
		
		// se queda con la "rama result"		
		$arreglo = $arreglo ['result'];
		$actualizacion = 0;
		foreach ($arreglo as $mensaje) {
			recibirMensaje ($mensaje['message']);
			$actualizacion = $mensaje['update_id'];
		}
		if ($actualizacion > 0)
			guardarNumeroActualizacion($actualizacion);
	}
	
	
	
	function actualizar ($actualizacion = 0) {
		$respuesta = obtenerActualizacion($actualizacion);
		verificarMensaje($respuesta);
	}
	
?>