<?php

	// CONFIGURAR ESTOS PARAMETROS CON LOS VALORES DE LA BASE DE DATOS
	
	// BASE DE DATOS REMOTA (SITIO WEB))
	define('bd_remoto_direccion', 'localhost');
	define('bd_remoto_usuario'  , 'usuariobd');
	define('bd_remoto_clave'    , '1234');
	define('bd_remoto_nombre'   , 'seguimedb');
	 	
	
	// BASE DE DATOS LOCAL (para pruebas o desarrollo)
	define('bd_local_direccion' , 'localhost');
	define('bd_local_usuario'   , 'root');
	define('bd_local_clave'     , '');
	define('bd_local_nombre'    , 'seguime');
	
	
	
	// ----------------------------------------------------------------
	
	
	
	
	
	
	
	// funciones 
	
	function bd_dato($dato) {
		
		if (local() == true) {
			if($dato == 'direccion') return bd_local_direccion;
			if($dato == 'usuario')   return bd_local_usuario;
			if($dato == 'clave')     return bd_local_clave;
			if($dato == 'nombre')    return bd_local_nombre;
		} else {     
	 		if($dato == 'direccion') return bd_remoto_direccion;
			if($dato == 'usuario')   return bd_remoto_usuario;
			if($dato == 'clave')     return bd_remoto_clave;
			if($dato == 'nombre')    return bd_remoto_nombre;
		}
		
		return '';
	}
	
	
	
	
	
	function local () {
		$direccion=$_SERVER['REMOTE_ADDR'];
		
		if (substr($direccion,0,8) == '192.168.')  return true; // trabaja en forma local		
		if (       $direccion      == 'localhost') return true; // trabaja en forma local		
		if (       $direccion      == '127.0.0.1') return true; // trabaja en forma local	
		if (       $direccion      == '::1')       return true; // trabaja en forma local
		
		return false; // trabaja en forma servidor		
	}
	

	

	
?>