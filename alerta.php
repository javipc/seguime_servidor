 <?php

	/*
	gestiona alertas en una base de datos

	revisa las alertas pendientes
	alertasObtener (tiempoActual)

	limpiar alertas vencidas
	alertaLimpiar(tiempoActual)

	nueva alerta
	alertaNueva(usuario, vencimiento, mensaje)

	contacto nuevo
	alertaContactoNuevo (usuario, idContacto, tipo)


	*/

	require_once('base.php');



	$bd = conectar();


	// revisa si existen Alertas "vencidas"
	// devuelve un arreglo con alertas vencidas

	function alertasObtener ($tiempo) {
		global $bd;

		$bdalerta = new BDDTabla ('alertas', $bd);
		$bdalerta->condicion ('vencimiento', '<=', $tiempo);
		$bdalerta->limite (5);

		$bdcontacto = new BDDTabla ('contactos', $bd);
		$bdcontacto->condicion ('tipo', 'telegram');

		$bdalerta->relacion ($bdcontacto, 'usuario');

		$bdalerta->seleccionar();

		$respuesta = array();
		while ($resultado = $bd->siguienteTupla())
			$respuesta[] = $resultado;

		return $respuesta;

	}




	// limpia las alertas vencidas,  y los contactos
	function alertaLimpiar ($tiempo) {

		global $bd;
		// consulta las alertas para obtener los usuarios, así borrar los contactos

		$bdalerta = new BDDTabla ('alertas', $bd);
		$bdalerta->condicion ('vencimiento', '<=', $tiempo);
		$bdalerta->limite (5);

		// elimina los contactos
		while ($resultado = $bd->siguienteTupla())
			alertaBorrarContactos($resultado['usuario']);

		// elimina las alertas
		$bdalerta->eliminar();
	}


	// elimina SOLO las alertas, pero según el usuario
	function alertaBorrar($usuario) {
		global $bd;

		$bdalerta = new BDDTabla ('alertas', $bd);
		$bdalerta->condicion ('usuario', $usuario);
		$bdalerta->eliminar();
	}


	// borra los contactos según el usuario
	function alertaBorrarContactos ($usuario) {
		global $bd;

		$bdcontacto = new BDDTabla ('contactos', $bd);
		$bdcontacto->condicion ('usuario', $usuario);
		$bdcontacto->eliminar();

	}








	function alertaNueva ($usuario, $vencimiento, $mensaje = '' ) {
		global $bd;

		$bdalerta = new BDDTabla ('alertas', $bd);
		$bdalerta->valor ('usuario'    , $usuario);
		$bdalerta->valor ('mensaje'    , $mensaje);
		$bdalerta->valor ('vencimiento', $vencimiento);
		$bdalerta->insertarSiNoExiste();
	}


	function alertaContactoNuevo ($usuario, $dato, $tipo='telegram') {

		global $bd;

		$bdcontacto = new BDDTabla ('contactos', $bd);
		$bdcontacto->valor ('usuario', $usuario);
		$bdcontacto->valor ('dato'   , $dato);
		$bdcontacto->valor ('tipo'   , $tipo);
		$bdcontacto->insertarSiNoExiste();

	}



	// calcula la fecha / hora de vencimiento, agregando a la hora el intervalo de tiempo
	function vencimiento($intervalo = '1 hour') {
		$ahora = ahora();
		return strtotime("$ahora + $intervalo");
	}







	// telegram
	//require_once('botseguime.php');


	function alertaEjecutar() {
		$ahora = ahora();
		// obtiene alertas vencidas
		foreach ( alertasObtener($ahora) as $alerta) {
			$telegram_id = $alerta['dato'];
			$mensaje     = $alerta['mensaje'];
			$vencimiento = $alerta['vencimiento'];
			$usuario     = $alerta['usuario'];

		//echo $vencimiento; echo "<br/>"; echo $usuario; echo "<br/>"; echo $telegram_id; echo "<br/>"; echo $mensaje;
			if ($telegram_id != '') {
				// echo'enviando mensaje';

				alertaBorrar ($usuario);
				alertaBorrarContactos($usuario);

				enviarMensaje ($telegram_id, "⚠️ MENSAJE DE ALERTA \n 👤 $usuario \n ✉️ $mensaje \n ⏱ $vencimiento UTC"); 
				// envia el mensaje al usuario de que se activó la alarma
				global $bd;
				$bdinstruccion = new BDDTabla ('instrucciones', $bd);
				$bdinstruccion->valor ('usuario', $usuario);
				$bdinstruccion->valor ('comando', 'mensaje');
				$bdinstruccion->valor ('parametro', "Se activó una alarma programada para $vencimiento");
				$bdinstruccion->insertar();
			}
		}

	}



 ?>
