<?php
	
	
	session_start();


	if (isset($_SESSION['usuario'])) 
		$usuario = $_SESSION['usuario'];
	else {
		header('Location: index.php?usuario=demo');
		die();
	}
	
	
	require_once('base.php');
	require_once('faphp.php');
	
	
			
	/*
	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
	*/
		
			
	

	
	
	
	
		
		
	// --------------------------------------------------
	// WEB
	// este archivo lo utiliza el usuario como sitio web
	// --------------------------------------------------

			
	

	$mensaje ='';


	// para desarrollo - visualiza formulario que imita comandos de la aplicación
	$formulario = dato('formulario');
	
	// recibe comandos 
	$comando    = dato('comando');
	$parametro  = dato('parametro');
	
	
	$imagen     = dato('imagen', '');	
	$fecha      = dato('fecha', '2017-20-7 13:15');
	$id         = dato('id', '0');
	
	// uso interno
	$control    = dato('control');
	
	
	
	// conecta con la base de datos
	$bd = conectar();
	
	
		
	
	
		// ultima imagen
		$bdimagen = new BDDTabla ('imagen', $bd);
		
		$bdimagen->condicion  ('usuario', $usuario);
		$bdimagen->orden      ('fecha'  , 'desc');		
		$bdimagen->limite     (1);
		
		if ($id > 0)
			$bdimagen->condicion  ('id', $id);
		$bdimagen->seleccionar();
		
		
		$respuesta = $bd->siguienteTupla();
		if (isset ($respuesta['imagen']))
			$imagen = $respuesta['imagen'];
		
		
	








	
	// servicio (utilizado por javascript para obtener imagenes nuevas sin recargar el php)
	if ($control == 'servicio') {
			
			
	}





	
			
			
			



	
	
		
		
	// cierra la sesion
	if ($control=='cerrarsesion') {
			session_destroy();
			header('Location: index.php');
			die();
	}
	
	
	
	
		
		
	// borra coordenadas
	if ($control=='borrarimagen') {
		
	}
	
	
	
	

	
	
	

			// visualiza la pagina
?>
		
<html>
	<head>
		<title>Seguime</title>
		<link rel="stylesheet" type="text/css" media="screen" href="esquema.css" />
		<link rel="stylesheet" type="text/css" media="screen" href="panel.css"  />
		<link rel="stylesheet" type="text/css" media="screen" href="movil.css"   />
		
		<meta name=viewport content="width=device-width, initial-scale=1" />
		<meta http-equiv="Content-Type" content="text/html;charset=utf8" />
		<link rel="shortcut icon" href="icono.png">
		
		
		
		
		
		
	</head>
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	<body>
			
		<div class='encabezado' >
			
				
				<span class=icono>
				<?php iconosvg(); ?>
				</span>
			<span>
				<h1>Seguime</h1>
			</span>	
				
			<span>
				<a href='imagenes.php'> 
				<span  class= 'rotable'>
				<?php 
					$svg = new faphp ();
					$svg->altura('30px');
					$svg->color('#fff');
					$svg->mostrar ('sync-alt');
				?>
				</span>
				</a>
			</span>
			
			<span>
				<a href='panel.php' > 
				<?php 					
					$svg->mostrar ('map');
				?>
				</a>
			</span>
			
			<span>
				<a href='opciones.php' > 
				<?php 					
					$svg->mostrar ('cog');
				?>
				</a>
			</span>
			
			<span>
				<a href='panel.php?control=cerrarsesion' class = 'rojofuerte'>
				<?php 					
					$svg->mostrar ('times');
				?>
				</a>
			</span>
			
			
			
		</div>
		
	










	
		<?php 
			if($mensaje != '')
				echo "<div class = 'cartel centrado'>
							<p class=mensaje>$mensaje</p>
						</div>";
			?>
		
		












		
		<div class= "tabla">
			<div class="fila">
				<div class="columna">
		
					<div class="imagen centrado" >
					
					
				<?php
					if ($imagen != '')
						echo "<img src = 'data:image/jpeg;base64,$imagen' />";
					
				?>
					</div>

			
						
					
					
					
			</div>
				
			<div class="columna">
			
	<?php		



				


				
				
				
							
				
				

				$etiqueta = new BDDEtiqueta() ;
				$etiqueta->atributo     ('href');
				$etiqueta->literal      ('imagenes.php');	
				$etiqueta->literal      ('?id=');
				$etiqueta->columna      ('id');	
				$etiqueta->finAtributo  ();
				
				// para javascript
				
				$etiqueta->atributo     ('meta-imagen');
				$etiqueta->columna      ('imagen');
				$etiqueta->finAtributo  ();
				
				
				
								
						
				$etiqueta->contenido    ();
				$etiqueta->literal      ('<img src=');
				$etiqueta->literal      ("\'");
				$etiqueta->literal  ('data:image/jpeg;base64,');
				$etiqueta->columna      ('imagen');
				$etiqueta->literal      ("\'");
				$etiqueta->literal      (' height=50 />');
				$etiqueta->finContenido ();
							
				$enlace =  'concat (' . $etiqueta->obtener('a') . ') as Imágen';
				
				//$enlaceCoordenada =  "concat ('<a href=\'panel.php?latitud=', latitud, '&longitud=', longitud   , '\'  class = \'tablaEnlace\' >', latitud, '|' , longitud,'</a>') as Posicion";
				
								
				
				// consulta de coordenadas
				$bdimagen = new BDDTabla ('imagen', $bd);
				$bdimagen->condicion('usuario', $usuario);
				$bdimagen->orden('fecha', 'desc');
				$bdimagen->limite(100);
				$bdimagen->seleccionar('fecha as Fecha', $enlace, 'extra as Extra');
				
				echo '<div class=table>';
				$bd->imprimir("border = '0'");
				echo '</div>';
				
				





				
		
			
				
				
		
		








	

	
	
?>
		

				</div>
			</div>
		</div>
		
			
	



<?php
			
					
					
	$bd->cerrar();
?>







	</body>
</html>
			
		

	
